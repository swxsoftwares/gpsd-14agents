CREATE ROLE monitor LOGIN
	ENCRYPTED PASSWORD 'md5a800338a5c32434bc98d647aacdb2926'
  	NOSUPERUSER INHERIT CREATEDB CREATEROLE REPLICATION;

CREATE DATABASE db_monitor
  	WITH OWNER = monitor
    	ENCODING = 'LATIN1'
       	TEMPLATE = template0
       	TABLESPACE = pg_default
       	LC_COLLATE = 'C'
       	LC_CTYPE = 'C'
       	CONNECTION LIMIT = -1;    

COMMENT ON DATABASE db_monitor
  	IS 'Base de dados para o registro de atividades de monitoramento dos recursos de hardware de um ou mais Nodes';

CREATE SEQUENCE sq_tb_node
 	INCREMENT	1
	MINVALUE 	1
 	START 		1;

ALTER TABLE sq_tb_node
  	OWNER TO monitor;

COMMENT ON SEQUENCE public.sq_tb_node
  	IS 'Sequence da tabela tb_node. Responsavel pelo valor automatico da coluna id_node';

CREATE TABLE tb_node (
	id_node 	INTEGER 		NOT NULL 	DEFAULT nextval('sq_tb_node')	PRIMARY KEY,
	nm_node		VARCHAR(100)	NOT NULL,
	dt_registro	TIMESTAMPTZ		NOT NULL,
	fl_ativo 	SMALLINT 		NOT NULL 	CHECK (fl_ativo IN (1,0)) 		DEFAULT 1,
	
	UNIQUE(nm_node)
);

ALTER TABLE tb_node
  	OWNER TO monitor;

COMMENT ON TABLE public.tb_node
  	IS 'Tabela que armazena os Nodes';
COMMENT ON COLUMN public.tb_node.id_node 
	IS 'Identificador do Node';
COMMENT ON COLUMN public.tb_node.nm_node 
	IS 'Nome do Node';
COMMENT ON COLUMN public.tb_node.dt_registro 
	IS 'Data de registro do Node';
COMMENT ON COLUMN public.tb_node.fl_ativo 
	IS 'Flag indicador que determina se o Node esta ativo (1) ou inativo (0)';

CREATE SEQUENCE sq_tb_node_pg
 	INCREMENT	1
	MINVALUE 	1
 	START 		1;

ALTER TABLE sq_tb_node_pg
  	OWNER TO monitor;

COMMENT ON SEQUENCE public.sq_tb_node_pg
  	IS 'Sequence da tabela tb_node_pg. Responsavel pelo valor automatico da coluna id_param_hist';

CREATE TABLE tb_node_pg (
	id_node					INTEGER			NOT NULL,
	id_param_hist			INTEGER			NOT NULL 	DEFAULT nextval('sq_tb_node_pg'),
	dt_registro 			TIMESTAMPTZ 	NOT NULl,
	ds_nome_comp_so			VARCHAR(100),
	ds_fqdn					VARCHAR(100),
	ds_plat					VARCHAR(100),
	ds_plat_versao			VARCHAR(100),
	ds_plat_familia			VARCHAR(100),
	ds_kern					VARCHAR(100),
	ds_kern_versao			VARCHAR(100),
	ds_kern_arquitetura		VARCHAR(100),
	ds_macaddress			VARCHAR(100),
	ds_ipaddress			VARCHAR(100),
	ds_ip6address			VARCHAR(100),
	ds_total_cpus			SMALLINT,
	ds_interf_disco			VARCHAR(100),
	ds_sist_arquiv_disco	VARCHAR(100),
	ds_interf_rede			VARCHAR(100),
	ds_dominio				VARCHAR(100),
	ds_grup_trab			VARCHAR(100),
	ds_uptime_seg			INTEGER,
	ds_ling_prog_disp		VARCHAR(1000),
	ds_versao_chef			VARCHAR(100),
	ds_versao_ohai			VARCHAR(100),
	ds_roles				VARCHAR(1000),
	ds_recipes				VARCHAR(1000),
	fl_ativo				SMALLINT		NOT NULL 	CHECK (fl_ativo IN (1,0))				DEFAULT 1,
	
	PRIMARY KEY(id_node, id_param_hist),
	FOREIGN KEY(id_node) REFERENCES tb_node(id_node)
);

ALTER TABLE tb_node_pg
  	OWNER TO monitor;

COMMENT ON TABLE public.tb_node_pg
  	IS 'Tabela que armazena o historico de capturas dos parametros gerais dos Nodes';
COMMENT ON COLUMN public.tb_node_pg.id_node 
	IS 'Identificador do Node';
COMMENT ON COLUMN public.tb_node_pg.id_param_hist 
	IS 'Identificador da tupla que contem os parametros gerais do Node';
COMMENT ON COLUMN public.tb_node_pg.dt_registro 
	IS 'Data de registro da tupla que contem os parametros gerais do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_nome_comp_so 
	IS 'Descricao do nome do computador definido no sistema operacional do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_fqdn 
	IS 'Descricao do FQDN do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_plat 
	IS 'Descricao da plataforma do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_plat_versao 
	IS 'Descricao da versao da plataforma do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_plat_familia 
	IS 'Descricao da familia da plataforma do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_kern 
	IS 'Descricao do kernel do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_kern_versao 
	IS 'Descricao da versao do kernel do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_kern_arquitetura 
	IS 'Descricao da arquitetura do kernel do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_macaddress 
	IS 'Descricao do endereco MAC do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_ipaddress 
	IS 'Descricao do endereco IPv4 do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_ip6address 
	IS 'Descricao do endereco IPv6 do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_total_cpus 
	IS 'Descricao do total de CPUs do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_interf_disco 
	IS 'Descricao da principal interface de disco do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_sist_arquiv_disco 
	IS 'Descricao do sistema de arquivos da principal interface de disco do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_interf_rede 
	IS 'Descricao da principal interface de rede do Node';
COMMENT ON COLUMN public.tb_node_pg.ds_dominio 
	IS 'Descricao do dominio ao qual pertence o Node';
COMMENT ON COLUMN public.tb_node_pg.ds_grup_trab 
	IS 'Descricao do grupo de trabalho ao qual pertence o Node';
COMMENT ON COLUMN public.tb_node_pg.ds_uptime_seg 
	IS 'Tempo, em segundos, que o Node esta ativo';
COMMENT ON COLUMN public.tb_node_pg.ds_ling_prog_disp 
	IS 'Descricao das linguagens de programacao disponiveis no Node';			
COMMENT ON COLUMN public.tb_node_pg.ds_versao_chef 
	IS 'Descricao da versao do Chef instalada no Node';
COMMENT ON COLUMN public.tb_node_pg.ds_versao_ohai 
	IS 'Descricao da versao do Ohai instalada no Node';
COMMENT ON COLUMN public.tb_node_pg.ds_roles 
	IS 'Descricao das roles que executam no Node';
COMMENT ON COLUMN public.tb_node_pg.ds_recipes 
	IS 'Descricao das recipes que executam no Node';
COMMENT ON COLUMN public.tb_node_pg.fl_ativo 
	IS 'Flag indicador que determina se a tupla do historico do Node esta ativa (1) ou inativa (0)';

CREATE SEQUENCE sq_tb_node_pe
	INCREMENT 		1
	MINVALUE 		1
	START 			1;

ALTER TABLE sq_tb_node_pe
  OWNER TO monitor;

COMMENT ON SEQUENCE public.sq_tb_node_pe
  	IS 'Sequence da tabela tb_node_pe. Responsavel pelo valor automatico da coluna id_param_hist';

CREATE TABLE tb_node_pe (
	id_node					INTEGER			NOT NULL,
	id_param_hist			INTEGER			NOT NULL 	DEFAULT nextval('sq_tb_node_pe'),
	dt_registro 			TIMESTAMPTZ 	NOT NULL,
	ds_cpu_porc_uso			SMALLINT,
	ds_mem_ram_total		INTEGER,
	ds_mem_ram_livre		INTEGER,
	ds_mem_swap_total		INTEGER,
	ds_mem_swap_livre		INTEGER,
	ds_rede_entrada_bytes	INTEGER,
	ds_rede_entrada_pac		INTEGER,
	ds_rede_saida_bytes		INTEGER,
	ds_rede_saida_pac		INTEGER,
	ds_disco_spaco_total	INTEGER,
	ds_disco_spaco_livre	INTEGER,
	ds_disco_porc_uso		SMALLINT,
	fl_ativo				INTEGER			NOT NULL 	CHECK (fl_ativo IN (1,0))			DEFAULT 1,
	
	PRIMARY KEY(id_node, id_param_hist),
	FOREIGN KEY(id_node) REFERENCES tb_node(id_node)
);

ALTER TABLE tb_node_pe
  	OWNER TO monitor;

COMMENT ON TABLE public.tb_node_pe
  	IS 'Tabela que armazena o historico de capturas dos parametros especificos (Memoria RAM, Memoria SWAP, Uso de Disco e Trafego de Rede) dos Nodes';
COMMENT ON COLUMN public.tb_node_pe.id_node 
	IS 'Identificador do Node';
COMMENT ON COLUMN public.tb_node_pe.id_param_hist
	IS 'Identificador da tupla que contem os parametros especificos do Node';
COMMENT ON COLUMN public.tb_node_pe.dt_registro
	IS 'Data de registro da tupla que contem os parametros especificos do Node';
COMMENT ON COLUMN public.tb_node_pe.ds_cpu_porc_uso
	IS 'Descricao da porcentagem de uso da CPU do Node';
COMMENT ON COLUMN public.tb_node_pe.ds_mem_ram_total
	IS 'Descricao do total de memoria RAM do Node';
COMMENT ON COLUMN public.tb_node_pe.ds_mem_ram_livre
	IS 'Descricao da memoria RAM disponivel no Node';
COMMENT ON COLUMN public.tb_node_pe.ds_mem_swap_total
	IS 'Descricao do total de memoria SWAP do Node';
COMMENT ON COLUMN public.tb_node_pe.ds_mem_swap_livre
	IS 'Descricao da memoria SWAP disponivel no Node';
COMMENT ON COLUMN public.tb_node_pe.ds_rede_entrada_bytes
	IS 'Descricao da quantidade de bytes de entrada da principal interface de rede do Node. (Essa interface de rede e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_rede_entrada_pac
	IS 'Descricao da quantidade de pacotes de entrada da principal interface de rede do Node. (Essa interface de rede e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_rede_saida_bytes
	IS 'Descricao da quantidade de bytes de saida da principal interface de rede do Node. (Essa interface de rede e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_rede_saida_pac
	IS 'Descricao da quantidade de pacotes de saida da principal interface de rede do Node. (Essa interface de rede e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_disco_spaco_total
	IS 'Descricao do total de espaco da principal interface de disco do Node. (Essa interface de disco e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_disco_spaco_livre
	IS 'Descricao do espaco disponivel na principal interface de disco do Node. (Essa interface de disco e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.ds_disco_porc_uso
	IS 'Descricao da porcentagem de uso da principal interface de disco do Node. (Essa interface de disco e listada nos parametros gerais do Node)';
COMMENT ON COLUMN public.tb_node_pe.fl_ativo
	IS 'Flag indicador que determina se a tupla do historico do Node esta ativa (1) ou inativa (0)';