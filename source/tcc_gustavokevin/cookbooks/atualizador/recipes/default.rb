#
# Cookbook Name:: my_deploy
# Recipe:: default
#
# Copyright 2014, cheff-training
#
# All rights reserved - Do Not Redistribute
#

if node["controlador_versao"] == "git" then
	if node["platform_family"] == "debian" then
		include_recipe 'apt'
	end

	include_recipe 'git'

	execute "git-config.user-name" do
		command "git config --global user.name '#{node["nome_usuario_controlador_versao"]}'"
	end

	execute "git-config.user-email" do
		command "git config --global user.email '#{node["email_usuario_controlador_versao"]}'"
	end

	if node["platform_family"] == "windows" then
		node["array_diretorios_deploy"].each do |diretorio|
			directory diretorio do
				rights :read, "Usuários"
				rights :full_control, "Administradores", :applies_to_children => true
			end
		end
	else 
		node["array_diretorios_deploy"].each do |diretorio|
			directory diretorio do
				mode 0755
			end
		end
	end

	if node["notificar_servidor_http_deploy"] then
		service node["nome_servidor_http_deploy"] do
  			supports :restart => true
  			action [ :enable, :start ]
		end
	end

	deploy_revision "minha_aplicacao" do
		deploy_to node["destino_deploy"]
		purge_before_symlink node["array_arquivos_excluir_deploy"]	
		symlink_before_migrate ({})
		create_dirs_before_symlink []
		symlinks ({})
		repo node["repositorio_deploy"]	
		revision node["revisao_deploy"]	
		user node["usuario_responsavel_deploy"]
	
		before_restart do
			link "aplicacao" do
				action :create
				owner node["usuario_responsavel_deploy"]
				target_file "#{node["diretorio_paginas_servidor_http_deploy"]}/minha_aplicacao.html"
				to "#{release_path}/minha_aplicacao.html"
			end
		end

		if node["notificar_servidor_http_deploy"] then
			notifies :restart, "service[#{node["nome_servidor_http_deploy"]}]"
		end
	end
end