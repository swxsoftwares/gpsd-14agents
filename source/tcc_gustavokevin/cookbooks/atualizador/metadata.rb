name             'atualizador'
maintainer       'cheff-training'
maintainer_email 'cheff-training@chefftraining.com'
license          'All rights reserved'
description      'Installs/Configures atualizador'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'apt'
depends 'git'