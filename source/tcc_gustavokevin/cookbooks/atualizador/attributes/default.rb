default["controlador_versao"] = "git" # Usar palavras minusculas
default["nome_usuario_controlador_versao"] = "Kevin Filipe"
default["email_usuario_controlador_versao"] = "kevinfillipe@gmail.com"


if node["platform_family"] == "windows" then
	default["array_diretorios_deploy"] = ["C:/atualizador"]
	default["destino_deploy"] = "C:/atualizador"
	default["usuario_responsavel_deploy"] = "Windows"
else
	default["array_diretorios_deploy"] = ["/opt", "/opt/atualizador"]
	default["destino_deploy"] = "/opt/atualizador"
	default["usuario_responsavel_deploy"] = "root"
end

default["array_arquivos_excluir_deploy"] = [".gitignore", "LICENSE", "README.md"]
default["repositorio_deploy"] = "https://github.com/kevinfilipe/chef-deploy.git"
default["revisao_deploy"] = "HEAD";

default["notificar_servidor_http_deploy"] = true

if node["platform_family"] == "rhel" then
	default["nome_servidor_http_deploy"] = "httpd"
else
	default["nome_servidor_http_deploy"] = "apache2"
end

if node["platform_family"] == "windows" then
	default["diretorio_paginas_servidor_http_deploy"] = "C:/apache2/www"
else
	default["diretorio_paginas_servidor_http_deploy"] = "/var/www"
end