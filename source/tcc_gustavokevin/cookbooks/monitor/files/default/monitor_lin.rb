require_relative './mod_monitor'

# 2 - Verificar execucoes do script

caminho_script = Monitor.obter_atributo_ohai("monitor/script_dir")

if !caminho_script.nil? then
	caminho_arquivo_configuracoes = File.join(caminho_script, 'monitor.conf')

	quantidade_execucoes_script = Monitor.obter_quantidade_execucoes_script(caminho_arquivo_configuracoes)

	if !quantidade_execucoes_script.nil? then
		# 3 - Capturar informacoes especificas dos componentes de hardware de um Node

		# 3.1 - Portentagem de uso da CPU
		cpu_porcentagem_uso = Monitor.obter_atributo_ohai("cpu/all/usr", true) 

		# 3.2 - Memoria RAM
		memoria_ram_total = Monitor.obter_atributo_ohai("memory/total", true)
		memoria_ram_livre = Monitor.obter_atributo_ohai("memory/free", true)

		# 3.3 - Memoria SWAP
		memoria_swap_total = Monitor.obter_atributo_ohai("memory/swap/total", true)
		memoria_swap_livre = Monitor.obter_atributo_ohai("memory/swap/free", true)

		# 3.4 - Uso de Disco
		interface_disco = Monitor.obter_atributo_ohai("monitor/script_interface_disco")
		disco_total = nil
		disco_livre = nil
		disco_porcentagem_uso = nil
		disco_tipo_arquivos = nil

		ohai_colecao = Monitor.obter_atributo_ohai("filesystem", false, true)

		if !interface_disco.nil? && !ohai_colecao.nil? then
			hash = JSON.parse(ohai_colecao)

			if !hash.nil? && !hash.empty? then
				disco_total = hash[interface_disco]["kb_size"]
				disco_livre = hash[interface_disco]["kb_available"]
				disco_porcentagem_uso = hash[interface_disco]["percent_used"].gsub("%", "")
				disco_tipo_arquivos = hash[interface_disco]["fs_type"]
			end
		end

		# 3.5 - Trafego de Rede
		interface_rede = Monitor.obter_atributo_ohai("network/default_interface")
		rede_entrada_bytes = nil
		rede_entrada_pacotes = nil
		rede_saida_bytes = nil
		rede_saida_pacotes = nil

		if !interface_rede.nil? then
			rede_entrada_bytes = Monitor.obter_atributo_ohai("counters/network/interfaces/#{interface_rede}/rx/bytes", true) 
			rede_entrada_pacotes = Monitor.obter_atributo_ohai("counters/network/interfaces/#{interface_rede}/rx/packets", true) 
			rede_saida_bytes = Monitor.obter_atributo_ohai("counters/network/interfaces/#{interface_rede}/tx/bytes", true) 
			rede_saida_pacotes = Monitor.obter_atributo_ohai("counters/network/interfaces/#{interface_rede}/tx/packets", true)
		end

		# 4 - Armazenar as informacoes capturadas (passo 1) em arquivos

		caminho_arquivo_param_especificos = File.join(caminho_script, "param-especificos.bd")
		
		parametros_especificos = [cpu_porcentagem_uso, memoria_ram_total, memoria_ram_livre, memoria_swap_total, memoria_swap_livre, rede_entrada_bytes, 
			rede_entrada_pacotes, rede_saida_bytes, rede_saida_pacotes, disco_total, disco_livre, disco_porcentagem_uso]

		continuar_execucao = Monitor.salvar_param_especificos_arquivo(caminho_arquivo_param_especificos, parametros_especificos)

		if continuar_execucao then
			# 5 - De tempos em tempos, enviar informacoes para um Banco de Dados

			quantidade_maxima_execucoes_script = Monitor.obter_atributo_ohai("monitor/script_max_exec", true)

			if !quantidade_maxima_execucoes_script.nil? then
				if quantidade_execucoes_script >= quantidade_maxima_execucoes_script then
					# 5.1 - Capturar informacoes gerais de um Node

					# 5.1.1 - Nome do Node
					nome_node = Monitor.obter_atributo_ohai("monitor/script_nome_node")

					# 5.1.2 - Nome do Computador
					nome_computador_so = Monitor.obter_atributo_ohai("machinename")

					# 5.1.3 - FQDN
					fqdn = Monitor.obter_atributo_ohai("fqdn")

					# 5.1.4 - Plataforma
					plataforma = Monitor.obter_atributo_ohai("platform")

					# 5.1.5 - Versao da Plataforma
					plataforma_versao = Monitor.obter_atributo_ohai("platform_version")

					# 5.1.6 - Familia da Plataforma
					plataforma_familia = Monitor.obter_atributo_ohai("platform_family")

					# 5.1.7 - Nome do Kernel
					kernel_nome = Monitor.obter_atributo_ohai("kernel/name")

					# 5.1.8 - Release do Kernel
					kernel_release = Monitor.obter_atributo_ohai("kernel/release")

					# 5.1.9 - Arquitetura do Kernel
					kernel_arquitetura = Monitor.obter_atributo_ohai("kernel/machine")

					# 5.1.10 - Endereco MAC
					macaddress = Monitor.obter_atributo_ohai("macaddress")

					# 5.1.11 - Endereco IP
					ipaddress = Monitor.obter_atributo_ohai("ipaddress")

					# 5.1.12 - Endereco IPv6
					ip6address = Monitor.obter_atributo_ohai("ip6address")

					# 5.1.13 - Dominio
					dominio = Monitor.obter_atributo_ohai("domain")

					# 5.1.14 - Endereco IPv6
					grupo_trabalho = Monitor.obter_atributo_ohai("kernel/cs_info/workgroup")

					# 5.1.15 - Endereco IPv6
					uptime_segundos = Monitor.obter_atributo_ohai("uptime_seconds", true)

					# 5.1.16 - Endereco IPv6
					linguagens_programacao = nil

					ohai_colecao = Monitor.obter_atributo_ohai("languages", false, true)
					
					if !ohai_colecao.nil? then
						hash = JSON.parse(ohai_colecao)

						if !hash.nil? && !hash.empty? then
							linguagens_programacao = ""

							hash.each do |chave, valor|
								linguagens_programacao = linguagens_programacao + chave + " "
							end

							linguagens_programacao.strip!
						end
					end

					# 5.1.17 - Total de CPUs
					cpu_total = Monitor.obter_atributo_ohai("cpu/total", true)

					# 5.1.18 - Versao do Chef
					versao_chef = Monitor.obter_atributo_ohai("chef_packages/chef/version")

					# 5.1.19 - Versao do Ohai
					versao_ohai = Monitor.obter_atributo_ohai("chef_packages/ohai/version")

					# 5.1.20 - Roles
					roles = nil

					ohai_colecao = Monitor.obter_atributo_ohai("monitor/script_roles_node", false, true)
					
					if !ohai_colecao.nil? then
						hash = JSON.parse(ohai_colecao)

						if !hash.nil? && !hash.empty? then
							roles = ""

							hash.each do |chave, valor|
								roles = roles + chave + " "
							end

							roles.strip!
						end
					end

					# 5.1.21 - Recipes
					recipes = nil

					ohai_colecao = Monitor.obter_atributo_ohai("monitor/script_recipes_node", false, true)

					if !ohai_colecao.nil? then
						hash = JSON.parse(ohai_colecao)

						if !hash.nil? && !hash.empty? then
							recipes = ""

							hash.each do |chave, valor|
								recipes = recipes + chave + " "
							end

							recipes.strip!
						end
					end

					# 5.2 - Enviar o conjunto completo de informacoes para um Banco de Dados

					conexao = Monitor.obter_conexao_banco_de_dados

					if !conexao.nil? then
						id_node = Monitor.verificar_node_banco_de_dados(nome_node, conexao)

						if !id_node.nil? then
							parametros_gerais = [id_node, Monitor.obter_data_execucao_script, nome_computador_so, fqdn, plataforma, plataforma_versao, 
								plataforma_familia, kernel_nome, kernel_release, kernel_arquitetura, macaddress, ipaddress, ip6address, cpu_total, 
								interface_disco, disco_tipo_arquivos, interface_rede, dominio, grupo_trabalho, uptime_segundos, linguagens_programacao, 
								versao_chef, versao_ohai, roles, recipes]

							continuar_execucao = Monitor.salvar_param_gerais_banco_de_dados(parametros_gerais, conexao)

							if continuar_execucao then
								continuar_execucao = Monitor.salvar_param_especificos_banco_de_dados(id_node, caminho_arquivo_param_especificos, conexao)
								
								if continuar_execucao then
									Monitor.truncar_arquivo_param_especificos(caminho_arquivo_param_especificos)
								
									Monitor.limpar_arquivo_configuracoes(caminho_arquivo_configuracoes)
								end
							end
						end

						Monitor.fechar_conexao_banco_de_dados(conexao)
					end
				end
			end
		end
	end
end