module Monitor
	# 1 - Carregar informacoes necessarias a execucao do modulo princpal do script

	# 1.1 - Dependencias
	require 'date'
	require 'open3'

	require 'rubygems'
	require 'pg'
	require 'json'

	# 1.1 - Data da Execucao do Script
	DATA_EXECUCAO_SCRIPT = DateTime.now

	# 1.2 - Nivel de Log do Script [0 - Nenhum | 1 - Basico | 2 - Completo]
	NIVEL_LOG_SCRIPT = 1

	# 1. 3 - Metodos
	def Monitor.obter_data_execucao_script
		return DATA_EXECUCAO_SCRIPT
	end

	def Monitor.obter_data_execucao_script_formatada
		return DATA_EXECUCAO_SCRIPT.strftime("%d/%m/%Y %H:%M:%S")
	end

	def Monitor.obter_atributo_ohai(chave_atributo, numerico = false, colecao = false)
		Open3.popen3 "ohai #{chave_atributo}" do |stdin, stdout, stderr, thread|
			if thread.value.success? then
				if numerico then
					resultado = stdout.read.match(/[[:digit:]]+/)

					if resultado.nil? then
						return nil
					else
						return resultado[0].to_i
					end
				elsif colecao then
					return stdout.read
				else
					resultado = stdout.read.match(/"(.*)"/)

					if resultado.nil? then
						return nil
					else
						return resultado[1]
					end
				end
			else
				Monitor.gerar_log_script(stderr.read, "Nao foi possivel obter o atributo '#{chave_atributo}' " + 
					"[Numerico: #{if numerico then 'Sim' else 'Nao' end} | Colecao: #{if colecao then 'Sim' else 'Nao' end}]")

				return nil
			end
		end

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel obter o atributo '#{chave_atributo}' " + 
					"[Numerico: #{if numerico then 'Sim' else 'Nao' end} | Colecao: #{if colecao then 'Sim' else 'Nao' end}]")

			return nil
	end

	def Monitor.salvar_param_especificos_arquivo(caminho_arquivo_param_especificos, parametros)
	 	linha_parametros = "#{DATA_EXECUCAO_SCRIPT.to_s} "

	 	parametros.each do |parametro|
			linha_parametros = linha_parametros + if parametro.nil? then "-" else parametro.to_s end + " "
		end

		linha_parametros = linha_parametros.strip!

	 	if File.exist?(caminho_arquivo_param_especificos) then
			arquivo_param_especificos = File.open(caminho_arquivo_param_especificos, 'a')

			arquivo_param_especificos.puts(linha_parametros)
			arquivo_param_especificos.close()
		else
			arquivo_param_especificos = File.new(caminho_arquivo_param_especificos, 'w', 0644)

			arquivo_param_especificos.puts(linha_parametros)
			arquivo_param_especificos.close()
		end

		return true

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel salvar os parametros especificos do node no arquivo de destino")

			return false
	end

	def Monitor.obter_quantidade_execucoes_script(caminho_arquivo_configuracoes)
		quantidade_execucoes_script = 1

		if File.exist?(caminho_arquivo_configuracoes) then
			arquivo_configuracoes = File.open(caminho_arquivo_configuracoes, 'r')

			quantidade_execucoes_script = arquivo_configuracoes.read.to_i + 1
			
			arquivo_configuracoes.close()

			arquivo_configuracoes = File.open(caminho_arquivo_configuracoes, 'w')

			arquivo_configuracoes.truncate(0)
			arquivo_configuracoes.write(quantidade_execucoes_script)
			arquivo_configuracoes.close()
		else
			arquivo_configuracoes = File.new(caminho_arquivo_configuracoes, 'w', 0644)

			arquivo_configuracoes.write(quantidade_execucoes_script)
			arquivo_configuracoes.close()
		end

		return quantidade_execucoes_script

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel verificar a quantidade de execucoes do script")

			return nil
	end

	def Monitor.limpar_arquivo_configuracoes(caminho_arquivo_configuracoes)
		arquivo_configuracoes = File.open(caminho_arquivo_configuracoes, 'w')

		arquivo_configuracoes.truncate(0)
		arquivo_configuracoes.write(0)
		arquivo_configuracoes.close()

		rescue
			Monitor.gerar_log_script(erro, "Nao foi possivel limpar o arquivo de configuracoes")	
	end

	def Monitor.obter_conexao_banco_de_dados
		conexao = PG::Connection.new(
			:hostaddr => Monitor.obter_atributo_ohai("monitor/script_bd/hostaddr"),
			:port => Monitor.obter_atributo_ohai("monitor/script_bd/port", true),
			:dbname => Monitor.obter_atributo_ohai("monitor/script_bd/dbname"),
			:user => Monitor.obter_atributo_ohai("monitor/script_bd/user"),
			:password => Monitor.obter_atributo_ohai("monitor/script_bd/password"))

		if !conexao.nil? && !conexao.finished? then
			if conexao.status == PG::Connection::CONNECTION_OK then
				return conexao
			else
				conexao.close
			end
		end

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel conectar-se ao banco de dados")

		return nil
	end

	def Monitor.fechar_conexao_banco_de_dados(conexao)
		if !conexao.nil? && !conexao.finished? then
			conexao.close
		end

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel fechar a conexao com o banco de dados")
	end

	def Monitor.verificar_node_banco_de_dados(nome_node, conexao)
		id_node = 0

		resultado = conexao.exec_params("SELECT n.id_node FROM tb_node n WHERE n.nm_node = $1 and n.fl_ativo = 1", [nome_node])

		resultado.each do |tupla|
			id_node = tupla["id_node"]
		end

		resultado.clear

		if id_node == 0 then
			conexao.exec_params("INSERT INTO tb_node (nm_node, dt_registro) VALUES ($1, $2)", [nome_node, Monitor.obter_data_execucao_script])

			resultado = conexao.exec_params("SELECT n.id_node FROM tb_node n WHERE n.nm_node = $1 and n.fl_ativo = 1", [nome_node])

			resultado.each do |tupla|
				id_node = tupla["id_node"]
			end

			resultado.clear
		end

		return id_node

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel verificar o node no banco de dados")	

			return nil
	end

	def Monitor.salvar_param_gerais_banco_de_dados(parametros_gerais, conexao)
		conexao.exec_params("INSERT INTO tb_node_pg (id_node, dt_registro, ds_nome_comp_so, ds_fqdn, ds_plat, ds_plat_versao, ds_plat_familia, ds_kern, ds_kern_versao, ds_kern_arquitetura, ds_macaddress, ds_ipaddress, ds_ip6address, ds_total_cpus, ds_interf_disco, ds_sist_arquiv_disco, ds_interf_rede, ds_dominio, ds_grup_trab, ds_uptime_seg, ds_ling_prog_disp, ds_versao_chef, ds_versao_ohai, ds_roles, ds_recipes) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25)",
			parametros_gerais)

		return true
		
		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel salvar os parametros gerais do node no banco de dados")	

			return false
	end

	def Monitor.salvar_param_especificos_banco_de_dados(id_node, caminho_arquivo_param_especificos, conexao)
		arquivo_param_especificos = File.open(caminho_arquivo_param_especificos, "r")

		arquivo_param_especificos.each_line do |linha|
			parametros = [id_node] + linha.split(" ")

			conexao.exec_params("INSERT INTO tb_node_pe (id_node, dt_registro, ds_cpu_porc_uso, ds_mem_ram_total, ds_mem_ram_livre, ds_mem_swap_total, ds_mem_swap_livre, ds_rede_entrada_bytes, ds_rede_entrada_pac, ds_rede_saida_bytes, ds_rede_saida_pac, ds_disco_spaco_total, ds_disco_spaco_livre, ds_disco_porc_uso) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)", 
				converter_colecao_padrao_banco_de_dados(parametros))
		end

		arquivo_param_especificos.close()

		return true

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel salvar os parametros especificos do node no banco de dados")

			return false
	end

	def Monitor.truncar_arquivo_param_especificos(caminho_arquivo)
		arquivo = File.open(caminho_arquivo, 'w')

		arquivo.truncate(0)
		arquivo.close()

		rescue => erro
			Monitor.gerar_log_script(erro, "Nao foi possivel truncar o arquivo de parametros especificos do node")
	end

	def Monitor.converter_colecao_padrao_banco_de_dados(colecao)
		colecao_bd = Array.new(colecao.size - 1)

		for indice in 0..(colecao.size - 1) do
			if colecao[indice].class == String then
				if colecao[indice] == "-" then
					colecao_bd[indice] = nil
				else
					colecao_bd[indice] = colecao[indice]
				end
			else
				colecao_bd[indice] = colecao[indice]
			end
		end

		return colecao_bd
	end

	def Monitor.gerar_log_script(erro, mensagem_log)
		if NIVEL_LOG_SCRIPT == 1 then
			puts obter_data_execucao_script_formatada + " - ATENCAO: " + mensagem_log;
		elsif NIVEL_LOG_SCRIPT == 2 then
			puts obter_data_execucao_script_formatada + " - ATENCAO: " + mensagem_log + ". Detalhe do erro:";
			
			if(erro.class == String) then
				puts erro
			else
				puts erro.message
			end

			puts
		end
	end
end