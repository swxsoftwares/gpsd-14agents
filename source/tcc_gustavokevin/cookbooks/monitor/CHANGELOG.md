monitor CHANGELOG
=================

This file is used to list changes made in each version of the monitor cookbook.

0.1.0
-----
- [Windows] - Initial release of monitor