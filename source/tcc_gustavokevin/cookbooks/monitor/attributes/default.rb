default["monitor_script_nome_node"] = Chef::Config[:node_name]
default["monitor_script_max_exec"] = "15"

case node["platform_family"]
when "windows"
	default["monitor_script_depend_gems"] = ["pg", "json"]

	default["monitor_chef_bin"] = File.join(File.dirname(Chef::Config.embedded_dir), 'bin').gsub("/", "\\")
	default["monitor_chef_embedded_bin"] = File.join(Chef::Config.embedded_dir, 'bin').gsub("/", "\\")
	default["monitor_ohai_plugins_path"] = File.join(Ohai::Config[:plugin_path]).gsub("/", "\\")
	
	default["monitor_script_dir"] = "C:/monitor"
	default["array_monitor_script_dir"] = ["C:/monitor"]

	default["usuario_execucao_monitor_script"] = "Windows"
	default["senha_usuario_execucao_monitor_script"] = "123456"
else
	case node["platform_family"] 
	when "debian"
		default["monitor_script_depend_pacotes"] = ["build-essential", "openssl", "libssl-dev", "ruby-dev", "zlib1g", "zlib1g-dev", "sysstat", "libpq-dev"]
	else
		default["monitor_script_depend_pacotes"] = ["gcc", "openssl", "openssl-devel", "zlib", "zlib-devel", "sysstat", "postgresql-devel"]
	end

	default["monitor_script_depend_gems"] = ["pg", "json"]

	default["monitor_chef_bin"] = File.join(File.dirname(Chef::Config.embedded_dir), 'bin')
	default["monitor_chef_embedded_bin"] = File.join(Chef::Config.embedded_dir, 'bin')
	default["monitor_ohai_plugins_path"] = File.join(Ohai::Config[:plugin_path])
	
	default["monitor_script_dir"] = "/opt/monitor"
	default["array_monitor_script_dir"] = ["/opt", "/opt/monitor"]

	case node["platform"]
	when "debian"
		default["monitor_script_interface_disco"] = "rootfs"
	else
		default["monitor_script_interface_disco"] = "/dev/sda2"
	end
end

default["monitor_script_bd"]["hostaddr"] = "192.168.0.7"
default["monitor_script_bd"]["port"] = "5432"
default["monitor_script_bd"]["dbname"] = "db_monitor"
default["monitor_script_bd"]["user"] = "monitor"
default["monitor_script_bd"]["password"] = "monitor"