#
# Cookbook Name:: monitor
# Recipe:: default
#
# Copyright 2014, Cheff-Training
#
# All rights reserved - Do Not Redistribute
#

# Dependencias - Pacotes
if node["platform_family"]  != "windows" then
	if node["platform_family"] == "debian" then
		include_recipe 'apt'
	end
	
	node["monitor_script_depend_pacotes"].each do |pacote|
		package pacote do
			action :install
		end
	end
end

# Dependencias - Gems
node["monitor_script_depend_gems"].each do |pacote_gem|
	gem_package pacote_gem do
		action :install
	end
end

if node["platform_family"]  == "windows" then
	# Configuracao de diretorios
	node["array_monitor_script_dir"].each do |diretorio|
		directory diretorio do
			rights :read, "Usuários"
			rights :full_control, "Administradores", :applies_to_children => true
		end
	end

	# Importacao de arquivos
	template "#{node["monitor_ohai_plugins_path"]}\\monitor.rb" do
		source "monitor.rb.erb" 
		rights :read, "Usuários"
		rights :full_control, "Administradores", :applies_to_children => true
	end

	cookbook_file "#{node["monitor_script_dir"]}\\mod_monitor.rb" do
		source "mod_monitor.rb"
		rights :read, "Usuários"
		rights :full_control, "Administradores", :applies_to_children => true
	end

	cookbook_file "#{node["monitor_script_dir"]}\\monitor_win.rb" do
		source "monitor_win.rb"
		rights :read, "Usuários"
		rights :full_control, "Administradores", :applies_to_children => true
	end

	# Ajustes no path
	windows_path "#{node["monitor_chef_bin"]}" do
  		action :add
	end

	windows_path "#{node["monitor_chef_embedded_bin"]}" do
  		action :add
	end

	# Agendamento do script
	windows_task "Script_Monitor" do
  		user node["usuario_execucao_monitor_script"]
  		password node["senha_usuario_execucao_monitor_script"]
  		command %Q{cmd /C ruby #{node["monitor_script_dir"]}\\monitor_win.rb >> #{node["monitor_script_dir"]}\\monitor_win.log 2>&1}
  		run_level :highest
  		frequency :minute
	end
else
	# Configuracao de diretorios
	node["array_monitor_script_dir"].each do |diretorio|
		directory diretorio do
			mode 0755
		end
	end

	# Importacao de arquivos
	template "#{node["monitor_ohai_plugins_path"]}/monitor.rb" do
		source "monitor.rb.erb" 
		mode 0644
	end

	template "#{node["monitor_ohai_plugins_path"]}/cpu_usage.rb" do
		source "cpu_usage.rb.erb" 
		mode 0644
	end

	cookbook_file "#{node["monitor_script_dir"]}/mod_monitor.rb" do
		source "mod_monitor.rb"
		mode 0644
	end

	cookbook_file "#{node["monitor_script_dir"]}/monitor_lin.rb" do
		source "monitor_lin.rb"
		mode 0644
	end

	# Ajustes no path e Agendamento do script
	cron "Script_Monitor" do
		path "/bin:/sbin:/usr/bin:/usr/sbin:#{node["monitor_chef_bin"]}:#{node["monitor_chef_embedded_bin"]}"
		command %Q{ruby #{node["monitor_script_dir"]}/monitor_lin.rb >> #{node["monitor_script_dir"]}/monitor_lin.log 2>&1}
	end
end