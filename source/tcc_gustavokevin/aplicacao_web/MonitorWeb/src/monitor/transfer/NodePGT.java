package monitor.transfer;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class NodePGT implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idNode;
	private int idParamHist;
	private Date dtRegistro;
	private String dsNomeCompSO;
	private String dsFQDN;
	private String dsPlat;
	private String dsPlatVersao;
	private String dsPlatFamilia;
	private String dsKern;
	private String dsKernVersao;
	private String dsKernArquitetura;
	private String dsMacAddress;
	private String dsIpAddress;
	private String dsIp6Address;
	private byte dsTotalCpus;
	private String dsInterfDisco;
	private String dsSistArquivDisco;
	private String dsInterfRede;
	private String dsDominio;
	private String dsGrupTrab;
	private int dsUptimeSeg;
	private String dsLingProgDisp;
	private String dsVersaoChef;
	private String dsVersaoOhai;
	private String dsRoles;
	private String dsRecipes;
	private byte flAtivo;

	private static final String MSGATRIBUTOSEMVALOR = "N�o Dispon�vel";

	private Calendar dtLeituraFiltro;
	private int flPlataforma;

	public int getIdNode() {
		return idNode;
	}

	public void setIdNode(int idNode) {
		this.idNode = idNode;
	}

	public int getIdParamHist() {
		return idParamHist;
	}

	public void setIdParamHist(int idParamHist) {
		this.idParamHist = idParamHist;
	}

	public Date getDtRegistro() {
		return dtRegistro;
	}

	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}

	public String getDsNomeCompSO() {
		return dsNomeCompSO;
	}

	public String getDsNomeCompSOVisao() {
		if (dsNomeCompSO != null && !dsNomeCompSO.isEmpty()) {
			return dsNomeCompSO;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsNomeCompSO(String dsNomeCompSO) {
		this.dsNomeCompSO = dsNomeCompSO;
	}

	public String getDsFQDN() {
		return dsFQDN;
	}

	public String getDsFQDNVisao() {
		if (dsFQDN != null && !dsFQDN.isEmpty()) {
			return dsFQDN;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsFQDN(String dsFQDN) {
		this.dsFQDN = dsFQDN;
	}

	public String getDsPlat() {
		return dsPlat;
	}

	public String getDsPlatVisao() {
		if (dsPlat != null && !dsPlat.isEmpty()) {
			return dsPlat;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsPlat(String dsPlat) {
		this.dsPlat = dsPlat;
	}

	public String getDsPlatVersao() {
		return dsPlatVersao;
	}

	public String getDsPlatVersaoVisao() {
		if (dsPlatVersao != null && !dsPlatVersao.isEmpty()) {
			return dsPlatVersao;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsPlatVersao(String dsPlatVersao) {
		this.dsPlatVersao = dsPlatVersao;
	}

	public String getDsPlatFamilia() {
		return dsPlatFamilia;
	}

	public String getDsPlatFamiliaVisao() {
		if (dsPlatFamilia != null && !dsPlatFamilia.isEmpty()) {
			return dsPlatFamilia;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsPlatFamilia(String dsPlatFamilia) {
		this.dsPlatFamilia = dsPlatFamilia;
	}

	public String getDsKern() {
		return dsKern;
	}

	public String getDsKernVisao() {
		if (dsKern != null && !dsKern.isEmpty()) {
			return dsKern;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsKern(String dsKern) {
		this.dsKern = dsKern;
	}

	public String getDsKernVersao() {
		return dsKernVersao;
	}

	public String getDsKernVersaoVisao() {
		if (dsKernVersao != null && !dsKernVersao.isEmpty()) {
			return dsKernVersao;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsKernVersao(String dsKernVersao) {
		this.dsKernVersao = dsKernVersao;
	}

	public String getDsKernArquitetura() {
		return dsKernArquitetura;
	}

	public String getDsKernArquiteturaVisao() {
		if (dsKernArquitetura != null && !dsKernArquitetura.isEmpty()) {
			return dsKernArquitetura;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsKernArquitetura(String dsKernArquitetura) {
		this.dsKernArquitetura = dsKernArquitetura;
	}

	public String getDsMacAddress() {
		return dsMacAddress;
	}

	public String getDsMacAddressVisao() {
		if (dsMacAddress != null && !dsMacAddress.isEmpty()) {
			return dsMacAddress;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsMacAddress(String dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}

	public String getDsIpAddress() {
		return dsIpAddress;
	}

	public String getDsIpAddressVisao() {
		if (dsIpAddress != null && !dsIpAddress.isEmpty()) {
			return dsIpAddress;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsIpAddress(String dsIpAddress) {
		this.dsIpAddress = dsIpAddress;
	}

	public String getDsIp6Address() {
		return dsIp6Address;
	}

	public String getDsIp6AddressVisao() {
		if (dsIp6Address != null && !dsIp6Address.isEmpty()) {
			return dsIp6Address;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsIp6Address(String dsIp6Address) {
		this.dsIp6Address = dsIp6Address;
	}

	public byte getDsTotalCpus() {
		return dsTotalCpus;
	}

	public String getDsTotalCpusVisao() {
		if (dsTotalCpus > 0) {
			return String.valueOf(dsTotalCpus);
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsTotalCpus(byte dsTotalCpus) {
		this.dsTotalCpus = dsTotalCpus;
	}

	public String getDsInterfDisco() {
		return dsInterfDisco;
	}

	public String getDsInterfDiscoVisao() {
		if (dsInterfDisco != null && !dsInterfDisco.isEmpty()) {
			return dsInterfDisco;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsInterfDisco(String dsInterfDisco) {
		this.dsInterfDisco = dsInterfDisco;
	}

	public String getDsSistArquivDisco() {
		return dsSistArquivDisco;
	}

	public String getDsSistArquivDiscoVisao() {
		if (dsSistArquivDisco != null && !dsSistArquivDisco.isEmpty()) {
			return dsSistArquivDisco;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsSistArquivDisco(String dsSistArquivDisco) {
		this.dsSistArquivDisco = dsSistArquivDisco;
	}

	public String getDsInterfRede() {
		return dsInterfRede;
	}

	public String getDsInterfRedeVisao() {
		if (dsInterfRede != null && !dsInterfRede.isEmpty()) {
			return dsInterfRede;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsInterfRede(String dsInterfRede) {
		this.dsInterfRede = dsInterfRede;
	}

	public String getDsDominio() {
		return dsDominio;
	}

	public String getDsDominioVisao() {
		if (dsDominio != null && !dsDominio.isEmpty()) {
			return dsDominio;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsDominio(String dsDominio) {
		this.dsDominio = dsDominio;
	}

	public String getDsGrupTrab() {
		return dsGrupTrab;
	}

	public String getDsGrupTrabVisao() {
		if (dsGrupTrab != null && !dsGrupTrab.isEmpty()) {
			return dsGrupTrab;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsGrupTrab(String dsGrupTrab) {
		this.dsGrupTrab = dsGrupTrab;
	}

	public int getDsUptimeSeg() {
		return dsUptimeSeg;
	}

	public String getDsUptimeSegVisao() {
		if (dsUptimeSeg > 0) {
			int horas = (dsUptimeSeg / 3600);
			int minutos = ((dsUptimeSeg / 60) - (horas * 60));
			int segundos = (dsUptimeSeg - ((minutos * 60) + (horas * 3600)));

			return String.format("%02d horas, %02d minutos e %02d segundos",
					horas, minutos, segundos);
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsUptimeSeg(int dsUptimeSeg) {
		this.dsUptimeSeg = dsUptimeSeg;
	}

	public String getDsLingProgDisp() {
		return dsLingProgDisp;
	}

	public String[] getDsLingProgDispVisao() {
		if (dsLingProgDisp != null && !dsLingProgDisp.isEmpty()) {
			return dsLingProgDisp.split(" ");
		} else {
			return new String[0];
		}
	}

	public void setDsLingProgDisp(String dsLingProgDisp) {
		this.dsLingProgDisp = dsLingProgDisp;
	}

	public String getDsVersaoChef() {
		return dsVersaoChef;
	}

	public String getDsVersaoChefVisao() {
		if (dsVersaoChef != null && !dsVersaoChef.isEmpty()) {
			return dsVersaoChef;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsVersaoChef(String dsVersaoChef) {
		this.dsVersaoChef = dsVersaoChef;
	}

	public String getDsVersaoOhai() {
		return dsVersaoOhai;
	}

	public String getDsVersaoOhaiVisao() {
		if (dsVersaoOhai != null && !dsVersaoOhai.isEmpty()) {
			return dsVersaoOhai;
		} else {
			return MSGATRIBUTOSEMVALOR;
		}
	}

	public void setDsVersaoOhai(String dsVersaoOhai) {
		this.dsVersaoOhai = dsVersaoOhai;
	}

	public String getDsRoles() {
		return dsRoles;
	}

	public String[] getDsRolesVisao() {
		if (dsRoles != null && !dsRoles.isEmpty()) {
			return dsRoles.split(" ");
		} else {
			return new String[0];
		}
	}

	public void setDsRoles(String dsRoles) {
		this.dsRoles = dsRoles;
	}

	public String getDsRecipes() {
		return dsRecipes;
	}

	public String[] getDsRecipesVisao() {
		if (dsRecipes != null && !dsRecipes.isEmpty()) {
			return dsRecipes.split(" ");
		} else {
			return new String[0];
		}
	}

	public void setDsRecipes(String dsRecipes) {
		this.dsRecipes = dsRecipes;
	}

	public byte getFlAtivo() {
		return flAtivo;
	}

	public void setFlAtivo(byte flAtivo) {
		this.flAtivo = flAtivo;
	}

	public Calendar getDtLeituraFiltro() {
		return dtLeituraFiltro;
	}

	public void setDtLeituraFiltro(Calendar dtLeituraFiltro) {
		this.dtLeituraFiltro = dtLeituraFiltro;
	}

	public int getFlPlataforma() {
		return flPlataforma;
	}

	public void setFlPlataforma(int flPlataforma) {
		this.flPlataforma = flPlataforma;
	}
}
