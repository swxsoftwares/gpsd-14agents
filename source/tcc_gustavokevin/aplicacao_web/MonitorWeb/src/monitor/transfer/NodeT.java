package monitor.transfer;

import java.io.Serializable;
import java.util.Date;

public class NodeT implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idNode;
	private String nmNode;
	private Date dtRegistro;
	private byte flAtivo;

	private NodePET paramEspecificos;
	private NodePGT paramGerais;

	public NodeT() {
		paramEspecificos = new NodePET();
		paramGerais = new NodePGT();
	}

	public int getIdNode() {
		return idNode;
	}

	public void setIdNode(int idNode) {
		this.idNode = idNode;
	}

	public String getNmNode() {
		return nmNode;
	}

	public void setNmNode(String nmNode) {
		this.nmNode = nmNode;
	}

	public Date getDtRegistro() {
		return dtRegistro;
	}

	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}

	public byte getFlAtivo() {
		return flAtivo;
	}

	public void setFlAtivo(byte flAtivo) {
		this.flAtivo = flAtivo;
	}

	public NodePET getParamEspecificos() {
		return paramEspecificos;
	}

	public void setParamEspecificos(NodePET paramEspecificos) {
		this.paramEspecificos = paramEspecificos;
	}

	public NodePGT getParamGerais() {
		return paramGerais;
	}

	public void setParamGerais(NodePGT paramGerais) {
		this.paramGerais = paramGerais;
	}
}
