package monitor.transfer;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NodePET implements Serializable {

	private static final long serialVersionUID = 1L;

	private int idNode;
	private int idParamHist;
	private Date dtRegistro;
	private byte dsCPUPorcUso;
	private int dsMemRAMTotal;
	private int dsMemRAMLivre;
	private int dsMemSWAPTotal;
	private int dsMemSWAPLivre;
	private int dsRedeEntradaBytes;
	private int dsRedeEntradaPac;
	private int dsRedeSaidaBytes;
	private int dsRedeSaidaPac;
	private int dsDiscoEspacoTotal;
	private int dsDiscoEspacoLivre;
	private byte dsDiscoPorcUso;
	private byte flAtivo;

	private Calendar dtInicioLeituraFiltro;
	private Calendar dtFinalLeituraFiltro;

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm");

	public int getFlIndicadorUsoCPU() {
		if (dsCPUPorcUso <= 0) {
			return 0;
		} else {
			if (dsCPUPorcUso < 70) {
				return 1;
			} else if (dsCPUPorcUso < 90) {
				return 2;
			} else {
				return 3;
			}
		}
	}

	public int getFlIndicadorUsoMemRAM() {
		if (dsMemRAMTotal <= 0) {
			return 0;
		} else {
			int tercaParte = dsMemRAMTotal / 3;
			int emUso = dsMemRAMTotal - dsMemRAMLivre;

			if (emUso <= tercaParte) {
				return 1;
			} else if (emUso <= tercaParte * 2) {
				return 2;
			} else {
				return 3;
			}
		}
	}

	public int getFlIndicadorUsoMemSWAP() {
		if (dsMemSWAPTotal <= 0) {
			return 0;
		} else {
			int tercaParte = dsMemSWAPTotal / 3;
			int emUso = dsMemSWAPTotal - dsMemSWAPLivre;

			if (emUso <= tercaParte) {
				return 1;
			} else if (emUso <= tercaParte * 2) {
				return 2;
			} else {
				return 3;
			}
		}
	}

	public int getQuantidadeMemRAMUso() {
		return (dsMemRAMTotal - dsMemRAMLivre) / 1024;
	}

	public int getQuantidadeMemSWAPUso() {
		return (dsMemSWAPTotal - dsMemSWAPLivre) / 1024;
	}

	public int getFlIndicadorUsoDisco() {
		if (dsDiscoPorcUso <= 0) {
			return 0;
		} else {
			if (dsDiscoPorcUso < 70) {
				return 1;
			} else if (dsDiscoPorcUso < 90) {
				return 2;
			} else {
				return 3;
			}
		}
	}

	public int getIdNode() {
		return idNode;
	}

	public void setIdNode(int idNode) {
		this.idNode = idNode;
	}

	public int getIdParamHist() {
		return idParamHist;
	}

	public void setIdParamHist(int idParamHist) {
		this.idParamHist = idParamHist;
	}

	public Date getDtRegistro() {
		return dtRegistro;
	}

	public String getDtRegistroVisao() {
		return sdf.format(dtRegistro);
	}

	public void setDtRegistro(Date dtRegistro) {
		this.dtRegistro = dtRegistro;
	}

	public byte getDsCPUPorcUso() {
		return dsCPUPorcUso;
	}

	public void setDsCPUPorcUso(byte dsCPUPorcUso) {
		this.dsCPUPorcUso = dsCPUPorcUso;
	}

	public int getDsMemRAMTotal() {
		return dsMemRAMTotal;
	}

	public void setDsMemRAMTotal(int dsMemRAMTotal) {
		this.dsMemRAMTotal = dsMemRAMTotal;
	}

	public int getDsMemRAMLivre() {
		return dsMemRAMLivre;
	}

	public void setDsMemRAMLivre(int dsMemRAMLivre) {
		this.dsMemRAMLivre = dsMemRAMLivre;
	}

	public int getDsMemSWAPTotal() {
		return dsMemSWAPTotal;
	}

	public void setDsMemSWAPTotal(int dsMemSWAPTotal) {
		this.dsMemSWAPTotal = dsMemSWAPTotal;
	}

	public int getDsMemSWAPLivre() {
		return dsMemSWAPLivre;
	}

	public void setDsMemSWAPLivre(int dsMemSWAPLivre) {
		this.dsMemSWAPLivre = dsMemSWAPLivre;
	}

	public int getDsRedeEntradaBytes() {
		return dsRedeEntradaBytes;
	}

	public void setDsRedeEntradaBytes(int dsRedeEntradaBytes) {
		this.dsRedeEntradaBytes = dsRedeEntradaBytes;
	}

	public int getDsRedeEntradaPac() {
		return dsRedeEntradaPac;
	}

	public void setDsRedeEntradaPac(int dsRedeEntradaPac) {
		this.dsRedeEntradaPac = dsRedeEntradaPac;
	}

	public int getDsRedeSaidaBytes() {
		return dsRedeSaidaBytes;
	}

	public void setDsRedeSaidaBytes(int dsRedeSaidaBytes) {
		this.dsRedeSaidaBytes = dsRedeSaidaBytes;
	}

	public int getDsRedeSaidaPac() {
		return dsRedeSaidaPac;
	}

	public void setDsRedeSaidaPac(int dsRedeSaidaPac) {
		this.dsRedeSaidaPac = dsRedeSaidaPac;
	}

	public int getDsDiscoEspacoTotal() {
		return dsDiscoEspacoTotal;
	}

	public void setDsDiscoEspacoTotal(int dsDiscoEspacoTotal) {
		this.dsDiscoEspacoTotal = dsDiscoEspacoTotal;
	}

	public int getDsDiscoEspacoLivre() {
		return dsDiscoEspacoLivre;
	}

	public void setDsDiscoEspacoLivre(int dsDiscoEspacoLivre) {
		this.dsDiscoEspacoLivre = dsDiscoEspacoLivre;
	}

	public byte getDsDiscoPorcUso() {
		return dsDiscoPorcUso;
	}

	public void setDsDiscoPorcUso(byte dsDiscoPorcUso) {
		this.dsDiscoPorcUso = dsDiscoPorcUso;
	}

	public byte getFlAtivo() {
		return flAtivo;
	}

	public void setFlAtivo(byte flAtivo) {
		this.flAtivo = flAtivo;
	}

	public Calendar getDtInicioLeituraFiltro() {
		return dtInicioLeituraFiltro;
	}

	public void setDtInicioLeituraFiltro(Calendar dtInicioLeituraFiltro) {
		this.dtInicioLeituraFiltro = dtInicioLeituraFiltro;
	}

	public Calendar getDtFinalLeituraFiltro() {
		return dtFinalLeituraFiltro;
	}

	public void setDtFinalLeituraFiltro(Calendar dtFinalLeituraFiltro) {
		this.dtFinalLeituraFiltro = dtFinalLeituraFiltro;
	}
}
