package monitor.bean.conversor;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import monitor.bean.mensagem.MonitorBundle;

@FacesConverter("conversorStatus")
public class ConversorStatus implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if ((value == null) || (!value.matches("\\d+"))) {
			return null;
		} else {
			return Integer.parseInt(value);
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String valor = value.toString();

		if (valor.matches("\\d+")) {
			switch (Integer.parseInt(valor)) {
			case 0: {
				return obterChaveBundle("lbInativo");
			}
			case 1: {
				return obterChaveBundle("lbAtivo");
			}
			default: {
				return null;
			}
			}
		} else {
			return null;
		}
	}

	private final String obterChaveBundle(String key) {
		MonitorBundle text = new MonitorBundle();

		return text.getString(key);
	}
}