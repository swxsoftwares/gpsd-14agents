package monitor.bean;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import monitor.bean.mensagem.MonitorBundle;
import monitor.dao.NodeDAO;
import monitor.dao.NodePEDAO;
import monitor.dao.NodePGDAO;
import monitor.transfer.NodePET;
import monitor.transfer.NodePGT;
import monitor.transfer.NodeT;

@ManagedBean(name = "homeBean")
@ViewScoped
public class HomeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private MonitorBundle monitorBundle = new MonitorBundle();
	private Locale locale = BeanUtils.obterLocale();
	private TimeZone timeZone = BeanUtils.obterTimeZone();
	private final SimpleDateFormat sdfDHMS = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss", BeanUtils.obterLocale());
	private final SimpleDateFormat sdfHM = new SimpleDateFormat("HH:mm",
			BeanUtils.obterLocale());

	private List<NodeT> listaNodes;

	private int idNodeSelecionado;
	private String nmNodeSelecionado;

	private NodePGT paramGeraisNodeSelecionado;
	private Calendar dtLeituraParamGeraisFiltro;
	private List<SelectItem> listaLeiturasParamGeraisFiltro;
	private int idLeituraParamGeraisSelecionada;
	private boolean nenhumaLeituraParamGeraisEncontrada;

	private Calendar dtInicioLeituraParamEspecifFiltro;
	private Calendar dtFinalLeituraParamEspecifFiltro;
	private String dataGraficoMemoriaRAM;
	private int maxMemoriaRAM;
	private String dataGraficoMemoriaSWAP;
	private int maxMemoriaSWAP;
	private String dataGraficoHD;
	private int maxHD;
	private String dataGraficoRede;

	private boolean exibirDetalhesNode;

	public HomeBean() {
		try {
			consultarNodes();
		} catch (Exception ex) {
			listaNodes = new ArrayList<NodeT>();

			ex.printStackTrace();
		}
	}

	private void consultarNodes() throws SQLException {
		NodeDAO nDAO = new NodeDAO();
		listaNodes = nDAO.consultarTodosComUltimosParamEspecificos();
	}

	public void pollConsultaNodes() {
		try {
			consultarNodes();
		} catch (Exception ex) {
			listaNodes = new ArrayList<NodeT>();

			ex.printStackTrace();
		}
	}

	public void pollConsultaParamGeraisNode() {
		Calendar dataAtual = Calendar.getInstance(timeZone, locale);

		if (datasIguaisDia(dataAtual, dtLeituraParamGeraisFiltro)) {
			if (idLeituraParamGeraisSelecionada == 0) {

				try {
					carregarParamGeraisNodeSelecionado();
				} catch (Exception ex) {
					paramGeraisNodeSelecionado = new NodePGT();
					dtLeituraParamGeraisFiltro = Calendar.getInstance(timeZone,
							locale);
					listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();
					idLeituraParamGeraisSelecionada = 0;
					nenhumaLeituraParamGeraisEncontrada = true;

					ex.printStackTrace();
				}
			} else {
				consultarListaLeiturasParamGeraisPorData(false);
			}
		}
	}

	public void consultarDetalhesNode() {
		try {
			NodeT nT = BeanUtils.obterObjetoRequisicao("node");

			idNodeSelecionado = nT.getIdNode();
			nmNodeSelecionado = nT.getNmNode();

			carregarParamGeraisNodeSelecionado();
			carregarParamEspecificosNodeSelecionado();

			exibirDetalhesNode = true;
		} catch (Exception ex) {
			idNodeSelecionado = 0;
			nmNodeSelecionado = null;

			dtLeituraParamGeraisFiltro = Calendar.getInstance(timeZone, locale);
			idLeituraParamGeraisSelecionada = 0;
			paramGeraisNodeSelecionado = new NodePGT();
			listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();
			nenhumaLeituraParamGeraisEncontrada = true;

			montarGraficosParamEspecificos(null, false, false, true);

			exibirDetalhesNode = false;

			ex.printStackTrace();
		}

		BeanUtils.adicionarParametroCallBackContextoResposta(
				"exibirDetalhesNode", exibirDetalhesNode);
	}

	private void carregarParamGeraisNodeSelecionado() throws Exception {
		NodeT nT = new NodeT();
		nT.setIdNode(idNodeSelecionado);

		NodePGDAO nPGDAO = new NodePGDAO();
		NodePGT _nodePGT = nPGDAO.consultarUltimoPorIdNode(nT);

		if (_nodePGT != null && _nodePGT.getIdParamHist() != 0) {
			_nodePGT.setIdNode(idNodeSelecionado);
			_nodePGT.setDtLeituraFiltro(Calendar.getInstance(timeZone, locale));
			_nodePGT.getDtLeituraFiltro().setTime(_nodePGT.getDtRegistro());

			dtLeituraParamGeraisFiltro = Calendar.getInstance(timeZone, locale);
			dtLeituraParamGeraisFiltro.setTime(_nodePGT.getDtRegistro());
			idLeituraParamGeraisSelecionada = _nodePGT.getIdParamHist();

			paramGeraisNodeSelecionado = _nodePGT;
			nenhumaLeituraParamGeraisEncontrada = false;

			nPGDAO = new NodePGDAO();
			List<NodePGT> listaNodePGT = nPGDAO
					.consultarPorIdNodeDtRegistro(paramGeraisNodeSelecionado);

			listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();

			for (int i = 0; i < listaNodePGT.size(); i++) {
				listaLeiturasParamGeraisFiltro.add(new SelectItem(listaNodePGT
						.get(i).getIdParamHist(), sdfDHMS.format(
						listaNodePGT.get(i).getDtRegistro()).toString()));
			}
		} else {
			paramGeraisNodeSelecionado = new NodePGT();
			dtLeituraParamGeraisFiltro = Calendar.getInstance(timeZone, locale);
			listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();
			idLeituraParamGeraisSelecionada = 0;
			nenhumaLeituraParamGeraisEncontrada = true;
		}
	}

	private void carregarParamEspecificosNodeSelecionado() throws Exception {
		NodeT nT = new NodeT();
		nT.setIdNode(idNodeSelecionado);

		NodePEDAO nPEDAO = new NodePEDAO();
		List<NodePET> listaNodePET = nPEDAO.consultarDezUltimosPorIdNode(nT);

		montarGraficosParamEspecificos(listaNodePET, true, true, true);
	}

	private void montarGraficosParamEspecificos(List<NodePET> listaNodePET,
			boolean reverterListaParametros, boolean intervaloDataAutomatico,
			boolean dataCorrenteAposFalha) {
		StringBuilder sbMemRAM = new StringBuilder();
		sbMemRAM.append("{");
		sbMemRAM.append("'cols': [");
		sbMemRAM.append("{'id': 'tempo', 'label': 'Tempo', 'type': 'string'},");
		sbMemRAM.append("{'id': 'memoriaLivre', 'label': 'Mem�ria RAM Livre', 'type': 'number'},");
		sbMemRAM.append("{'id': 'memoriaUso', 'label': 'Mem�ria RAM em Uso', 'type': 'number'}");
		sbMemRAM.append("],");
		sbMemRAM.append("'rows': [");

		StringBuilder sbMemSWAP = new StringBuilder();
		sbMemSWAP.append("{");
		sbMemSWAP.append("'cols': [");
		sbMemSWAP
				.append("{'id': 'tempo', 'label': 'Tempo', 'type': 'string'},");
		sbMemSWAP
				.append("{'id': 'swapLivre', 'label': 'SWAP Livre', 'type': 'number'},");
		sbMemSWAP
				.append("{'id': 'swapUso', 'label': 'SWAP em Uso', 'type': 'number'}");
		sbMemSWAP.append("],");
		sbMemSWAP.append("'rows': [");

		StringBuilder sbHD = new StringBuilder();
		sbHD.append("{");
		sbHD.append("'cols': [");
		sbHD.append("{'id': 'tempo', 'label': 'Tempo', 'type': 'string'},");
		sbHD.append("{'id': 'hdLivre', 'label': 'HD Livre', 'type': 'number'},");
		sbHD.append("{'id': 'hdUso', 'label': 'HD em Uso', 'type': 'number'}");
		sbHD.append("],");
		sbHD.append("'rows': [");

		StringBuilder sbRede = new StringBuilder();
		sbRede.append("{");
		sbRede.append("'cols': [");
		sbRede.append("{'id': 'tempo', 'label': 'Tempo', 'type': 'string'},");
		sbRede.append("{'id': 'trafegoEntrada', 'label': 'Tr�fego de Entrada', 'type': 'number'},");
		sbRede.append("{'id': 'trafegoSaida', 'label': 'Tr�fego de Sa�da', 'type': 'number'}");
		sbRede.append("],");
		sbRede.append("'rows': [");

		int maxLeituraMemoriaRAM = 0;
		int maxLeituraMemoriaSWAP = 0;
		int maxLeituraHD = 0;

		if (listaNodePET != null && !listaNodePET.isEmpty()) {
			if (reverterListaParametros) {
				Collections.reverse(listaNodePET);
			}

			String data = "";
			int memRAMLivre = 0;
			int memRAMUso = 0;
			int memSWAPLivre = 0;
			int memSWAPUso = 0;
			int hdLivre = 0;
			int hdUso = 0;
			int redeEntrada = 0;
			int redeSaida = 0;

			for (int i = 0; i < listaNodePET.size(); i++) {
				data = sdfHM.format(listaNodePET.get(i).getDtRegistro());

				memRAMLivre = kiloBytesParaMegaBytes(listaNodePET.get(i)
						.getDsMemRAMLivre());
				memRAMUso = kiloBytesParaMegaBytes(listaNodePET.get(i)
						.getDsMemRAMTotal()
						- listaNodePET.get(i).getDsMemRAMLivre());
				memSWAPLivre = kiloBytesParaMegaBytes(listaNodePET.get(i)
						.getDsMemSWAPLivre());
				memSWAPUso = kiloBytesParaMegaBytes(listaNodePET.get(i)
						.getDsMemSWAPTotal()
						- listaNodePET.get(i).getDsMemSWAPLivre());
				hdLivre = megaBytesParaGigaBytes(listaNodePET.get(i)
						.getDsDiscoEspacoLivre());
				hdUso = megaBytesParaGigaBytes(listaNodePET.get(i)
						.getDsDiscoEspacoTotal()
						- listaNodePET.get(i).getDsDiscoEspacoLivre());
				redeEntrada = bytesParaMegaBytes(listaNodePET.get(i)
						.getDsRedeEntradaBytes());
				redeSaida = bytesParaMegaBytes(listaNodePET.get(i)
						.getDsRedeSaidaBytes());

				if (i == 0 && intervaloDataAutomatico) {
					dtInicioLeituraParamEspecifFiltro = new GregorianCalendar(
							timeZone, locale);
					dtInicioLeituraParamEspecifFiltro.setTime(listaNodePET.get(
							i).getDtRegistro());
				}

				if (maxLeituraMemoriaRAM < listaNodePET.get(i)
						.getDsMemRAMTotal()) {
					maxLeituraMemoriaRAM = listaNodePET.get(i)
							.getDsMemRAMTotal();
				}
				if (maxLeituraMemoriaSWAP < listaNodePET.get(i)
						.getDsMemSWAPTotal()) {
					maxLeituraMemoriaSWAP = listaNodePET.get(i)
							.getDsMemSWAPTotal();
				}
				if (maxLeituraHD < listaNodePET.get(i).getDsDiscoEspacoTotal()) {
					maxLeituraHD = listaNodePET.get(i).getDsDiscoEspacoTotal();
				}

				if (i == listaNodePET.size() - 1) {
					if (intervaloDataAutomatico) {
						dtFinalLeituraParamEspecifFiltro = new GregorianCalendar(
								timeZone, locale);
						dtFinalLeituraParamEspecifFiltro.setTime(listaNodePET
								.get(i).getDtRegistro());
					}

					sbMemRAM.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ memRAMLivre + ", 'f': '" + memRAMLivre
							+ " MiB'}, {'v': " + memRAMUso + ", 'f': '"
							+ memRAMUso + " MiB'}]}");
					sbMemSWAP.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ memSWAPLivre + ", 'f': '" + memSWAPLivre
							+ " MiB'}, {'v': " + memSWAPUso + ", 'f': '"
							+ memSWAPUso + " MiB'}]}");
					sbHD.append("{'c':[{'v': '" + data + "'}, {'v': " + hdLivre
							+ ", 'f': '" + hdLivre + " GiB'}, {'v': " + hdUso
							+ ", 'f': '" + hdUso + " GiB'}]}");
					sbRede.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ redeEntrada + ", 'f': '" + redeEntrada
							+ " MiB'}, {'v': " + redeSaida + ", 'f': '"
							+ redeSaida + " MiB'}]}");
				} else {
					sbMemRAM.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ memRAMLivre + ", 'f': '" + memRAMLivre
							+ " MiB'}, {'v': " + memRAMUso + ", 'f': '"
							+ memRAMUso + " MiB'}]},");
					sbMemSWAP.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ memSWAPLivre + ", 'f': '" + memSWAPLivre
							+ " MiB'}, {'v': " + memSWAPUso + ", 'f': '"
							+ memSWAPUso + " MiB'}]},");
					sbHD.append("{'c':[{'v': '" + data + "'}, {'v': " + hdLivre
							+ ", 'f': '" + hdLivre + " GiB'}, {'v': " + hdUso
							+ ", 'f': '" + hdUso + " GiB'}]},");
					sbRede.append("{'c':[{'v': '" + data + "'}, {'v': "
							+ redeEntrada + ", 'f': '" + redeEntrada
							+ " MiB'}, {'v': " + redeSaida + ", 'f': '"
							+ redeSaida + " MiB'}]},");
				}

				data = "";
				memRAMLivre = 0;
				memRAMUso = 0;
				memSWAPLivre = 0;
				memSWAPUso = 0;
				hdLivre = 0;
				hdUso = 0;
				redeEntrada = 0;
				redeSaida = 0;
			}
		} else {
			if (dataCorrenteAposFalha) {
				dtInicioLeituraParamEspecifFiltro = Calendar.getInstance(
						timeZone, locale);
				dtFinalLeituraParamEspecifFiltro = Calendar.getInstance(
						timeZone, locale);
			}
		}

		sbMemRAM.append("]");
		sbMemRAM.append("}");

		sbMemSWAP.append("]");
		sbMemSWAP.append("}");

		sbHD.append("]");
		sbHD.append("}");

		sbRede.append("]");
		sbRede.append("}");

		dataGraficoMemoriaRAM = sbMemRAM.toString().replace("'", "\"");
		maxMemoriaRAM = kiloBytesParaMegaBytes(maxLeituraMemoriaRAM);
		dataGraficoMemoriaSWAP = sbMemSWAP.toString().replace("'", "\"");
		maxMemoriaSWAP = kiloBytesParaMegaBytes(maxLeituraMemoriaSWAP);
		dataGraficoHD = sbHD.toString().replace("'", "\"");
		maxHD = megaBytesParaGigaBytes(maxLeituraHD);
		dataGraficoRede = sbRede.toString().replace("'", "\"");
	}

	private int megaBytesParaGigaBytes(int numero) {
		return kiloBytesParaMegaBytes(numero) / 1024;
	}

	private int kiloBytesParaMegaBytes(int numero) {
		return numero / 1024;
	}

	private int bytesParaMegaBytes(int numero) {
		return kiloBytesParaMegaBytes(numero) / 1024;
	}

	public void consultarListaLeiturasParamGeraisPorDataVisao() {
		consultarListaLeiturasParamGeraisPorData(true);
	}

	private void consultarListaLeiturasParamGeraisPorData(
			boolean limparVariaveis) {
		NodePGT nPGT = new NodePGT();
		nPGT.setIdNode(idNodeSelecionado);
		nPGT.setDtLeituraFiltro(dtLeituraParamGeraisFiltro);

		try {
			NodePGDAO nPGDAO = new NodePGDAO();
			List<NodePGT> listaNodePGT = nPGDAO
					.consultarPorIdNodeDtRegistro(nPGT);

			if (listaNodePGT != null && !listaNodePGT.isEmpty()) {
				nenhumaLeituraParamGeraisEncontrada = false;

				listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();

				if (limparVariaveis) {
					idLeituraParamGeraisSelecionada = 0;
				}

				for (int i = 0; i < listaNodePGT.size(); i++) {
					listaLeiturasParamGeraisFiltro.add(new SelectItem(
							listaNodePGT.get(i).getIdParamHist(),
							sdfDHMS.format(listaNodePGT.get(i).getDtRegistro())
									.toString()));

					if (i == 0 && limparVariaveis) {
						idLeituraParamGeraisSelecionada = listaNodePGT.get(i)
								.getIdParamHist();
					}
				}

				if (idLeituraParamGeraisSelecionada > 0 && limparVariaveis) {
					consultarParamGeraisPorIdHistParam();
				}
			} else {
				if (limparVariaveis) {
					idLeituraParamGeraisSelecionada = 0;
				}

				paramGeraisNodeSelecionado = new NodePGT();
				listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();
				nenhumaLeituraParamGeraisEncontrada = true;
			}
		} catch (Exception ex) {
			if (limparVariaveis) {
				idLeituraParamGeraisSelecionada = 0;
			}

			paramGeraisNodeSelecionado = new NodePGT();
			listaLeiturasParamGeraisFiltro = new ArrayList<SelectItem>();
			nenhumaLeituraParamGeraisEncontrada = true;

			ex.printStackTrace();
		}
	}

	private void consultarParamGeraisPorIdHistParam() throws Exception {
		NodePGT nPGT = new NodePGT();
		nPGT.setIdNode(idNodeSelecionado);
		nPGT.setIdParamHist(idLeituraParamGeraisSelecionada);

		NodePGDAO nPGDAO = new NodePGDAO();
		paramGeraisNodeSelecionado = nPGDAO.consultarPorIdNodeIdParamHist(nPGT);
	}

	public void consultarParamGeraisNodePorLeitura() {
		try {
			consultarParamGeraisPorIdHistParam();
		} catch (Exception ex) {
			paramGeraisNodeSelecionado = new NodePGT();

			ex.printStackTrace();
		}
	}

	public void consultarParamEspecificosPorData() {
		dtInicioLeituraParamEspecifFiltro.set(Calendar.SECOND, 0);
		dtFinalLeituraParamEspecifFiltro.set(Calendar.SECOND, 0);

		if (dataFimPosterior(dtInicioLeituraParamEspecifFiltro,
				dtFinalLeituraParamEspecifFiltro)) {
			montarGraficosParamEspecificos(null, false, false, false);

			return;
		}

		NodePET nPET = new NodePET();
		nPET.setIdNode(idNodeSelecionado);
		nPET.setDtInicioLeituraFiltro(dtInicioLeituraParamEspecifFiltro);
		nPET.setDtFinalLeituraFiltro(dtFinalLeituraParamEspecifFiltro);

		try {
			NodePEDAO nPEDAO = new NodePEDAO();
			List<NodePET> listaNodePET = nPEDAO
					.consultarPorIdNodeIntervaloDatas(nPET);

			if (listaNodePET != null && !listaNodePET.isEmpty()) {
				montarGraficosParamEspecificos(listaNodePET, false, false,
						false);
			} else {
				montarGraficosParamEspecificos(null, false, false, false);
			}
		} catch (Exception ex) {
			montarGraficosParamEspecificos(null, false, false, false);

			ex.printStackTrace();
		}
	}

	public void fecharDetalhesNode() {
		idNodeSelecionado = 0;
		nmNodeSelecionado = null;
		exibirDetalhesNode = false;

		BeanUtils.adicionarParametroCallBackContextoResposta(
				"exibirDetalhesNode", exibirDetalhesNode);
	}

	private boolean datasIguaisDia(Calendar calendarA, Calendar calendarB) {
		if ((calendarA.get(Calendar.DAY_OF_MONTH) == calendarB
				.get(Calendar.DAY_OF_MONTH))
				&& (calendarA.get(Calendar.MONTH) == calendarB
						.get(Calendar.MONTH))
				&& (calendarA.get(Calendar.YEAR) == calendarB
						.get(Calendar.YEAR))) {
			return true;
		} else {
			return false;
		}
	}

	private boolean dataFimPosterior(Calendar inicio, Calendar fim) {
		if ((fim.get(Calendar.DAY_OF_MONTH) < inicio.get(Calendar.DAY_OF_MONTH))
				&& (fim.get(Calendar.MONTH) < inicio.get(Calendar.MONTH))
				&& (fim.get(Calendar.YEAR) < inicio.get(Calendar.YEAR))
				&& (fim.get(Calendar.HOUR_OF_DAY) < inicio
						.get(Calendar.HOUR_OF_DAY))
				&& (fim.get(Calendar.MINUTE) < inicio.get(Calendar.MINUTE))) {
			return true;
		} else {
			return false;
		}
	}

	public List<NodeT> getListaNodes() {
		return listaNodes;
	}

	public void setListaNodes(List<NodeT> listaNodes) {
		this.listaNodes = listaNodes;
	}

	public int getIdNodeSelecionado() {
		return idNodeSelecionado;
	}

	public void setIdNodeSelecionado(int idNodeSelecionado) {
		this.idNodeSelecionado = idNodeSelecionado;
	}

	public String getNmNodeSelecionado() {
		return nmNodeSelecionado;
	}

	public void setNmNodeSelecionado(String nmNodeSelecionado) {
		this.nmNodeSelecionado = nmNodeSelecionado;
	}

	public NodePGT getParamGeraisNodeSelecionado() {
		return paramGeraisNodeSelecionado;
	}

	public void setParamGeraisNodeSelecionado(NodePGT paramGeraisNodeSelecionado) {
		this.paramGeraisNodeSelecionado = paramGeraisNodeSelecionado;
	}

	public Date getDtLeituraParamGeraisFiltro() {
		return dtLeituraParamGeraisFiltro.getTime();
	}

	public void setDtLeituraParamGeraisFiltro(Date dtLeituraParamGeraisFiltro) {
		this.dtLeituraParamGeraisFiltro.setTime(dtLeituraParamGeraisFiltro);
	}

	public List<SelectItem> getListaLeiturasParamGeraisFiltro() {
		return listaLeiturasParamGeraisFiltro;
	}

	public void setListaLeiturasParamGeraisFiltro(
			List<SelectItem> listaLeiturasParamGeraisFiltro) {
		this.listaLeiturasParamGeraisFiltro = listaLeiturasParamGeraisFiltro;
	}

	public int getIdLeituraParamGeraisSelecionada() {
		return idLeituraParamGeraisSelecionada;
	}

	public void setIdLeituraParamGeraisSelecionada(
			int idLeituraParamGeraisSelecionada) {
		this.idLeituraParamGeraisSelecionada = idLeituraParamGeraisSelecionada;
	}

	public boolean isNenhumaLeituraParamGeraisEncontrada() {
		return nenhumaLeituraParamGeraisEncontrada;
	}

	public void setNenhumaLeituraParamGeraisEncontrada(
			boolean nenhumaLeituraParamGeraisEncontrada) {
		this.nenhumaLeituraParamGeraisEncontrada = nenhumaLeituraParamGeraisEncontrada;
	}

	public Date getDtInicioLeituraParamEspecifFiltro() {
		return dtInicioLeituraParamEspecifFiltro.getTime();
	}

	public void setDtInicioLeituraParamEspecifFiltro(
			Date dtInicioLeituraParamEspecifFiltro) {
		this.dtInicioLeituraParamEspecifFiltro
				.setTime(dtInicioLeituraParamEspecifFiltro);
	}

	public Date getDtFinalLeituraParamEspecifFiltro() {
		return dtFinalLeituraParamEspecifFiltro.getTime();
	}

	public void setDtFinalLeituraParamEspecifFiltro(
			Date dtFinalLeituraParamEspecifFiltro) {
		this.dtFinalLeituraParamEspecifFiltro
				.setTime(dtFinalLeituraParamEspecifFiltro);
	}

	public String getDataGraficoMemoriaRAM() {
		return dataGraficoMemoriaRAM;
	}

	public void setDataGraficoMemoriaRAM(String dataGraficoMemoriaRAM) {
		this.dataGraficoMemoriaRAM = dataGraficoMemoriaRAM;
	}

	public int getMaxMemoriaRAM() {
		return maxMemoriaRAM;
	}

	public void setMaxMemoriaRAM(int maxMemoriaRAM) {
		this.maxMemoriaRAM = maxMemoriaRAM;
	}

	public String getDataGraficoMemoriaSWAP() {
		return dataGraficoMemoriaSWAP;
	}

	public void setDataGraficoMemoriaSWAP(String dataGraficoMemoriaSWAP) {
		this.dataGraficoMemoriaSWAP = dataGraficoMemoriaSWAP;
	}

	public int getMaxMemoriaSWAP() {
		return maxMemoriaSWAP;
	}

	public void setMaxMemoriaSWAP(int maxMemoriaSWAP) {
		this.maxMemoriaSWAP = maxMemoriaSWAP;
	}

	public String getDataGraficoHD() {
		return dataGraficoHD;
	}

	public void setDataGraficoHD(String dataGraficoHD) {
		this.dataGraficoHD = dataGraficoHD;
	}

	public String getDataGraficoRede() {
		return dataGraficoRede;
	}

	public void setDataGraficoRede(String dataGraficoRede) {
		this.dataGraficoRede = dataGraficoRede;
	}

	public boolean isExibirDetalhesNode() {
		return exibirDetalhesNode;
	}

	public void setExibirDetalhesNode(boolean exibirDetalhesNode) {
		this.exibirDetalhesNode = exibirDetalhesNode;
	}

	public int getMaxHD() {
		return maxHD;
	}

	public void setMaxHD(int maxHD) {
		this.maxHD = maxHD;
	}
}