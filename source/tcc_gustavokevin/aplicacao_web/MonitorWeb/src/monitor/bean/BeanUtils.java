package monitor.bean;

import java.util.Locale;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public class BeanUtils {
	protected final static Locale obterLocale() {
		LocaleBean localeBean = (LocaleBean) obterAtributoSessao("localeBean");

		return localeBean.getLocale();
	}

	protected final static TimeZone obterTimeZone() {
		LocaleBean localeBean = (LocaleBean) obterAtributoSessao("localeBean");

		return localeBean.getTimeZone();
	}

	protected final static void exibirMensagemVisao(
			FacesMessage.Severity tipoMensagem, String mensagem) {
		FacesContext fc = FacesContext.getCurrentInstance();

		fc.addMessage(null, new FacesMessage(tipoMensagem, mensagem, null));
	}

	@SuppressWarnings("unchecked")
	protected final static <T> T obterObjetoRequisicao(String key) {
		return (T) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(key);
	}

	protected final static void adicionarParametroCallBackContextoResposta(
			String nomeParametro, boolean valorParametro) {
		RequestContext contexto = RequestContext.getCurrentInstance();
		contexto.addCallbackParam(nomeParametro, valorParametro);
	}

	protected final static Object obterAtributoSessao(String key) {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get(key);
	}
}
