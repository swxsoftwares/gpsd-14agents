package monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoUtils {
	public static final String SERVER_NAME = "192.168.0.7";
	public static final int PORT_NUMBER = 5432;
	public static final String DATABASE_NAME = "db_monitor";
	public static final String USER = "monitor";
	public static final String PASSWORD = "monitor";

	public static final void fecharConexao(Connection connection)
			throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
		}
	}

	public static final void fecharPreparedStatement(PreparedStatement pStmt)
			throws SQLException {
		if (pStmt != null && !pStmt.isClosed()) {
			pStmt.close();
		}
	}

	public static final void fecharResultSet(ResultSet rs) throws SQLException {
		if (rs != null && !rs.isClosed()) {
			rs.close();
		}
	}

	public static final void fecharResultSetEPreparedStatement(ResultSet rs,
			PreparedStatement pStmt) throws SQLException {
		fecharResultSet(rs);
		fecharPreparedStatement(pStmt);
	}
}