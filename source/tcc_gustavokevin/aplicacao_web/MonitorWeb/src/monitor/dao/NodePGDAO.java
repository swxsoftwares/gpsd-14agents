package monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import monitor.transfer.NodePGT;
import monitor.transfer.NodeT;

public class NodePGDAO {
	private Connection connection;

	private static final String SQL_CONSULTAR_ULTIMO_POR_ID_NODE = "SELECT npg.id_param_hist, npg.dt_registro, npg.ds_nome_comp_so, npg.ds_fqdn, npg.ds_plat, npg.ds_plat_versao, npg.ds_plat_familia, npg.ds_kern, npg.ds_kern_versao, npg.ds_kern_arquitetura, npg.ds_macaddress, npg.ds_ipaddress, npg.ds_ip6address, npg.ds_total_cpus, npg.ds_interf_disco, npg.ds_sist_arquiv_disco, npg.ds_interf_rede, npg.ds_dominio, npg.ds_grup_trab, npg.ds_uptime_seg, npg.ds_ling_prog_disp, npg.ds_versao_chef, npg.ds_versao_ohai, npg.ds_roles, npg.ds_recipes FROM tb_node_pg npg WHERE npg.id_node = ? AND npg.fl_ativo = 1 ORDER BY npg.id_param_hist DESC LIMIT 1";
	private static final String SQL_CONSULTAR_POR_ID_NODE_ID_PARAM_HIST = "SELECT npg.dt_registro, npg.ds_nome_comp_so, npg.ds_fqdn, npg.ds_plat, npg.ds_plat_versao, npg.ds_plat_familia, npg.ds_kern, npg.ds_kern_versao, npg.ds_kern_arquitetura, npg.ds_macaddress, npg.ds_ipaddress, npg.ds_ip6address, npg.ds_total_cpus, npg.ds_interf_disco, npg.ds_sist_arquiv_disco, npg.ds_interf_rede, npg.ds_dominio, npg.ds_grup_trab, npg.ds_uptime_seg, npg.ds_ling_prog_disp, npg.ds_versao_chef, npg.ds_versao_ohai, npg.ds_roles, npg.ds_recipes FROM tb_node_pg npg WHERE npg.id_node = ? AND npg.id_param_hist = ?";
	private static final String SQL_CONSULTAR_POR_ID_NODE_DT_REGISTRO = "SELECT npg.id_param_hist, npg.dt_registro FROM tb_node_pg npg WHERE npg.id_node = ? AND npg.dt_registro::date = ?::date AND npg.fl_ativo = 1 ORDER BY npg.id_param_hist DESC";

	public NodePGDAO() throws SQLException {
		connection = FabricaConexoes.obterConexao();
	}

	public NodePGT consultarUltimoPorIdNode(NodeT nT) throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_ULTIMO_POR_ID_NODE);
			pStmt.setObject(1, nT.getIdNode());

			rs = pStmt.executeQuery();

			return converterResultsetParaLista1(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	public NodePGT consultarPorIdNodeIdParamHist(NodePGT nPGT)
			throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_POR_ID_NODE_ID_PARAM_HIST);
			pStmt.setObject(1, nPGT.getIdNode());
			pStmt.setObject(2, nPGT.getIdParamHist());

			rs = pStmt.executeQuery();

			return converterResultsetParaLista2(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	public List<NodePGT> consultarPorIdNodeDtRegistro(NodePGT nPGT)
			throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_POR_ID_NODE_DT_REGISTRO);
			pStmt.setObject(1, nPGT.getIdNode());
			pStmt.setObject(2, new java.sql.Date(nPGT.getDtLeituraFiltro()
					.getTimeInMillis()));

			rs = pStmt.executeQuery();

			return converterResultsetParaLista3(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	private NodePGT converterResultsetParaLista1(ResultSet rs)
			throws SQLException {
		NodePGT nPGT = new NodePGT();

		if (rs.next()) {
			nPGT.setIdParamHist(rs.getInt("ID_PARAM_HIST"));
			nPGT.setDtRegistro(rs.getTimestamp("DT_REGISTRO"));
			nPGT.setDsNomeCompSO(rs.getString("DS_NOME_COMP_SO"));
			nPGT.setDsFQDN(rs.getString("DS_FQDN"));
			nPGT.setDsPlat(rs.getString("DS_PLAT"));
			nPGT.setDsPlatVersao(rs.getString("DS_PLAT_VERSAO"));
			nPGT.setDsPlatFamilia(rs.getString("DS_PLAT_FAMILIA"));
			nPGT.setDsKern(rs.getString("DS_KERN"));
			nPGT.setDsKernVersao(rs.getString("DS_KERN_VERSAO"));
			nPGT.setDsKernArquitetura(rs.getString("DS_KERN_ARQUITETURA"));
			nPGT.setDsMacAddress(rs.getString("DS_MACADDRESS"));
			nPGT.setDsIpAddress(rs.getString("DS_IPADDRESS"));
			nPGT.setDsIp6Address(rs.getString("DS_IP6ADDRESS"));
			nPGT.setDsTotalCpus(rs.getByte("DS_TOTAL_CPUS"));
			nPGT.setDsInterfDisco(rs.getString("DS_INTERF_DISCO"));
			nPGT.setDsSistArquivDisco(rs.getString("DS_SIST_ARQUIV_DISCO"));
			nPGT.setDsInterfRede(rs.getString("DS_INTERF_REDE"));
			nPGT.setDsDominio(rs.getString("DS_DOMINIO"));
			nPGT.setDsGrupTrab(rs.getString("DS_GRUP_TRAB"));
			nPGT.setDsUptimeSeg(rs.getInt("DS_UPTIME_SEG"));
			nPGT.setDsLingProgDisp(rs.getString("DS_LING_PROG_DISP"));
			nPGT.setDsVersaoChef(rs.getString("DS_VERSAO_CHEF"));
			nPGT.setDsVersaoOhai(rs.getString("DS_VERSAO_OHAI"));
			nPGT.setDsRoles(rs.getString("DS_ROLES"));
			nPGT.setDsRecipes(rs.getString("DS_RECIPES"));
		}

		return nPGT;
	}

	private NodePGT converterResultsetParaLista2(ResultSet rs)
			throws SQLException {
		NodePGT nPGT = new NodePGT();

		if (rs.next()) {
			nPGT.setDtRegistro(rs.getTimestamp("DT_REGISTRO"));
			nPGT.setDsNomeCompSO(rs.getString("DS_NOME_COMP_SO"));
			nPGT.setDsFQDN(rs.getString("DS_FQDN"));
			nPGT.setDsPlat(rs.getString("DS_PLAT"));
			nPGT.setDsPlatVersao(rs.getString("DS_PLAT_VERSAO"));
			nPGT.setDsPlatFamilia(rs.getString("DS_PLAT_FAMILIA"));
			nPGT.setDsKern(rs.getString("DS_KERN"));
			nPGT.setDsKernVersao(rs.getString("DS_KERN_VERSAO"));
			nPGT.setDsKernArquitetura(rs.getString("DS_KERN_ARQUITETURA"));
			nPGT.setDsMacAddress(rs.getString("DS_MACADDRESS"));
			nPGT.setDsIpAddress(rs.getString("DS_IPADDRESS"));
			nPGT.setDsIp6Address(rs.getString("DS_IP6ADDRESS"));
			nPGT.setDsTotalCpus(rs.getByte("DS_TOTAL_CPUS"));
			nPGT.setDsInterfDisco(rs.getString("DS_INTERF_DISCO"));
			nPGT.setDsSistArquivDisco(rs.getString("DS_SIST_ARQUIV_DISCO"));
			nPGT.setDsInterfRede(rs.getString("DS_INTERF_REDE"));
			nPGT.setDsDominio(rs.getString("DS_DOMINIO"));
			nPGT.setDsGrupTrab(rs.getString("DS_GRUP_TRAB"));
			nPGT.setDsUptimeSeg(rs.getInt("DS_UPTIME_SEG"));
			nPGT.setDsLingProgDisp(rs.getString("DS_LING_PROG_DISP"));
			nPGT.setDsVersaoChef(rs.getString("DS_VERSAO_CHEF"));
			nPGT.setDsVersaoOhai(rs.getString("DS_VERSAO_OHAI"));
			nPGT.setDsRoles(rs.getString("DS_ROLES"));
			nPGT.setDsRecipes(rs.getString("DS_RECIPES"));
		}

		return nPGT;
	}

	private List<NodePGT> converterResultsetParaLista3(ResultSet rs)
			throws SQLException {
		List<NodePGT> listaNodePGT = new ArrayList<NodePGT>();

		while (rs.next()) {
			NodePGT nPGT = new NodePGT();
			nPGT.setIdParamHist(rs.getInt("ID_PARAM_HIST"));
			nPGT.setDtRegistro(rs.getTimestamp("DT_REGISTRO"));

			listaNodePGT.add(nPGT);
		}

		return listaNodePGT;
	}
}