package monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import monitor.transfer.NodePET;
import monitor.transfer.NodeT;

public class NodePEDAO {
	private Connection connection;

	private static final String SQL_CONSULTAR_DEZ_ULTIMOS_POR_ID_NODE = "SELECT npe.dt_registro, npe.ds_cpu_porc_uso, npe.ds_mem_ram_total, npe.ds_mem_ram_livre, npe.ds_mem_swap_total, npe.ds_mem_swap_livre, npe.ds_rede_entrada_bytes, npe.ds_rede_saida_bytes, npe.ds_disco_spaco_total, npe.ds_disco_spaco_livre, npe.ds_disco_porc_uso FROM tb_node_pe npe WHERE npe.id_node = ? AND npe.fl_ativo = 1 ORDER BY npe.id_param_hist DESC LIMIT 10";
	private static final String SQL_CONSULTAR_POR_ID_NODE_INTERVALO_DATAS = "SELECT npe.dt_registro, npe.ds_cpu_porc_uso, npe.ds_mem_ram_total, npe.ds_mem_ram_livre, npe.ds_mem_swap_total, npe.ds_mem_swap_livre, npe.ds_rede_entrada_bytes, npe.ds_rede_saida_bytes, npe.ds_disco_spaco_total, npe.ds_disco_spaco_livre, npe.ds_disco_porc_uso FROM tb_node_pe npe WHERE npe.id_node = ? AND date_trunc('minute', npe.dt_registro::timestamp) BETWEEN date_trunc('minute', ?::timestamp) AND date_trunc('minute', ?::timestamp) AND npe.fl_ativo = 1";

	public NodePEDAO() throws SQLException {
		connection = FabricaConexoes.obterConexao();
	}

	public List<NodePET> consultarDezUltimosPorIdNode(NodeT nT)
			throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_DEZ_ULTIMOS_POR_ID_NODE);
			pStmt.setObject(1, nT.getIdNode());

			rs = pStmt.executeQuery();

			return converterResultsetParaLista2(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	public List<NodePET> consultarPorIdNodeIntervaloDatas(NodePET nPET)
			throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_POR_ID_NODE_INTERVALO_DATAS);
			pStmt.setObject(1, nPET.getIdNode());
			pStmt.setObject(2, new java.sql.Timestamp(nPET
					.getDtInicioLeituraFiltro().getTime().getTime()));
			pStmt.setObject(3, new java.sql.Timestamp(nPET
					.getDtFinalLeituraFiltro().getTime().getTime()));

			rs = pStmt.executeQuery();

			return converterResultsetParaLista2(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	private List<NodePET> converterResultsetParaLista2(ResultSet rs)
			throws SQLException {
		List<NodePET> listaNodePET = new ArrayList<NodePET>();

		while (rs.next()) {
			NodePET nPET = new NodePET();
			nPET.setDtRegistro(rs.getTimestamp("DT_REGISTRO"));
			nPET.setDsCPUPorcUso(rs.getByte("DS_CPU_PORC_USO"));
			nPET.setDsMemRAMTotal(rs.getInt("DS_MEM_RAM_TOTAL"));
			nPET.setDsMemRAMLivre(rs.getInt("DS_MEM_RAM_LIVRE"));
			nPET.setDsMemSWAPTotal(rs.getInt("DS_MEM_SWAP_TOTAL"));
			nPET.setDsMemSWAPLivre(rs.getInt("DS_MEM_SWAP_LIVRE"));
			nPET.setDsRedeEntradaBytes(rs.getInt("DS_REDE_ENTRADA_BYTES"));
			nPET.setDsRedeSaidaBytes(rs.getInt("DS_REDE_SAIDA_BYTES"));
			nPET.setDsDiscoEspacoTotal(rs.getInt("DS_DISCO_SPACO_TOTAL"));
			nPET.setDsDiscoEspacoLivre(rs.getInt("DS_DISCO_SPACO_LIVRE"));
			nPET.setDsDiscoPorcUso(rs.getByte("DS_DISCO_PORC_USO"));

			listaNodePET.add(nPET);
		}

		return listaNodePET;
	}
}
