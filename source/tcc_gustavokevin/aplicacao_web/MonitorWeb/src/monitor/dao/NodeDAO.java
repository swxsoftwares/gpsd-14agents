package monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import monitor.transfer.NodeT;

public class NodeDAO {
	private Connection connection;

	private static final String SQL_CONSULTAR_TODOS_COM_ULTIMOS_PARAM_ESPECIFICOS = "SELECT n.id_node, n.nm_node, n.dt_registro, n.fl_ativo, (SELECT npe.ds_cpu_porc_uso FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_cpu_porc_uso, (SELECT npe.ds_mem_ram_total FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_mem_ram_total, (SELECT npe.ds_mem_ram_livre FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_mem_ram_livre, (SELECT npe.ds_mem_swap_total FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_mem_swap_total, (SELECT npe.ds_mem_swap_livre FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_mem_swap_livre, (SELECT npe.ds_disco_porc_uso FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as ds_disco_porc_uso,(SELECT CASE WHEN lower(ds_plat_familia) = 'windows' THEN 1 WHEN lower(ds_kern) = 'linux' THEN 2 ELSE 3 END FROM tb_node_pg npg WHERE n.id_node = npg.id_node ORDER BY npg.id_param_hist DESC LIMIT 1) fl_plataforma, (SELECT npe.dt_registro FROM tb_node_pe npe WHERE n.id_node = npe.id_node ORDER BY npe.id_param_hist DESC LIMIT 1) as dt_leitura FROM tb_node n ORDER BY n.nm_node";

	public NodeDAO() throws SQLException {
		connection = FabricaConexoes.obterConexao();
	}

	public List<NodeT> consultarTodosComUltimosParamEspecificos()
			throws SQLException {
		PreparedStatement pStmt = null;
		ResultSet rs = null;

		try {
			pStmt = connection
					.prepareStatement(SQL_CONSULTAR_TODOS_COM_ULTIMOS_PARAM_ESPECIFICOS);

			rs = pStmt.executeQuery();

			return converterResultsetParaLista(rs);
		} finally {
			DaoUtils.fecharResultSetEPreparedStatement(rs, pStmt);
			DaoUtils.fecharConexao(connection);
		}
	}

	private List<NodeT> converterResultsetParaLista(ResultSet rs)
			throws SQLException {
		List<NodeT> listaNodeT = new ArrayList<NodeT>();

		while (rs.next()) {
			NodeT nT = new NodeT();

			nT.setIdNode(rs.getInt("ID_NODE"));
			nT.setNmNode(rs.getString("NM_NODE"));
			nT.setDtRegistro(rs.getTimestamp("DT_REGISTRO"));
			nT.setFlAtivo(rs.getByte("FL_ATIVO"));

			nT.getParamEspecificos().setDsCPUPorcUso(
					rs.getByte("DS_CPU_PORC_USO"));
			nT.getParamEspecificos().setDsMemRAMTotal(
					rs.getInt("DS_MEM_RAM_TOTAL"));
			nT.getParamEspecificos().setDsMemRAMLivre(
					rs.getInt("DS_MEM_RAM_LIVRE"));
			nT.getParamEspecificos().setDsMemSWAPTotal(
					rs.getInt("DS_MEM_SWAP_TOTAL"));
			nT.getParamEspecificos().setDsMemSWAPLivre(
					rs.getInt("DS_MEM_SWAP_LIVRE"));
			nT.getParamEspecificos().setDsDiscoPorcUso(
					rs.getByte("DS_DISCO_PORC_USO"));
			nT.getParamEspecificos().setDtRegistro(
					rs.getTimestamp("DT_LEITURA"));

			nT.getParamGerais().setFlPlataforma(rs.getInt("FL_PLATAFORMA"));

			listaNodeT.add(nT);
		}

		return listaNodeT;
	}
}
