package monitor.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.postgresql.ds.PGPoolingDataSource;

public class FabricaConexoes {

	private static PGPoolingDataSource pgPoolDataSource;

	public final static Connection obterConexao() throws SQLException {
		return obterInstanciaDataSource().getConnection();
	}

	private final static PGPoolingDataSource obterInstanciaDataSource()
			throws SQLException {
		if (pgPoolDataSource == null) {
			return obterDataSource();
		} else {
			return pgPoolDataSource;
		}
	}

	private final static PGPoolingDataSource obterDataSource()
			throws SQLException {
		pgPoolDataSource = new PGPoolingDataSource();

		pgPoolDataSource.setServerName(DaoUtils.SERVER_NAME);
		pgPoolDataSource.setUser(DaoUtils.USER);
		pgPoolDataSource.setPassword(DaoUtils.PASSWORD);
		pgPoolDataSource.setPortNumber(DaoUtils.PORT_NUMBER);
		pgPoolDataSource.setDatabaseName(DaoUtils.DATABASE_NAME);

		return pgPoolDataSource;
	}
}