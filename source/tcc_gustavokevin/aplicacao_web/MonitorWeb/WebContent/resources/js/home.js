function desenharGraficoMemoriaRam() {
	var data = document.getElementById("hiddenDataGraficoMemoriaRAM").value;

	var dataTable = new google.visualization.DataTable(data);

	var options = {
		legend : {
			position : 'bottom'
		},
		vAxis : {
			format : '# MiB',
			minValue : 0,
			maxValue : document.getElementById("hiddenMaxMemoriaRAM").value
		}
	};

	var chart = new google.visualization.AreaChart(document
			.getElementById('grafico-memoria-ram'));
	chart.draw(dataTable, options);
};

function desenharGraficoMemoriaSwap() {
	var data = document.getElementById("hiddenDataGraficoMemoriaSWAP").value;

	var dataTable = new google.visualization.DataTable(data);

	var options = {
		legend : {
			position : 'bottom'
		},
		vAxis : {
			format : '# MiB',
			minValue : 0,
			maxValue : document.getElementById("hiddenMaxMemoriaSWAP").value
		}
	};

	var chart = new google.visualization.AreaChart(document
			.getElementById('grafico-memoria-swap'));
	chart.draw(dataTable, options);
};

function desenharGraficoHD() {
	var data = document.getElementById("hiddenDataGraficoHD").value;

	var dataTable = new google.visualization.DataTable(data);

	var options = {
		legend : {
			position : 'bottom'
		},
		vAxis : {
			format : '# GiB',
			minValue : 0,
			maxValue : document.getElementById("hiddenMaxHD").value
		}
	};

	var chart = new google.visualization.AreaChart(document
			.getElementById('grafico-hd'));
	chart.draw(dataTable, options);
};

function desenharGraficoRede() {
	var data = document.getElementById("hiddenDataGraficoRede").value;

	var dataTable = new google.visualization.DataTable(data);

	var options = {
		legend : {
			position : 'bottom'
		},
		vAxis : {
			format : '# MiB',
			minValue : 0
		}
	};

	var chart = new google.visualization.AreaChart(document
			.getElementById('grafico-rede'));
	chart.draw(dataTable, options);
};

function listenerPoll(xhr, status, args) {
	if (args.exibirDetalhesNode) {
		desenharGraficoMemoriaRam();
		desenharGraficoMemoriaSwap();
		desenharGraficoHD();
		desenharGraficoRede();

		PF('pollParamGeraisNode').start();
	} else {
		PF('pollParamGeraisNode').stop();
	}
};